import { commonValidators } from '../src/util/functions';

describe('validators', () => {
  test('email', () => {
    expect(commonValidators.email('not an email')).toBe(false);
    expect(commonValidators.email('test@gmail')).toBe(false);
    expect(commonValidators.email('test @gmail .com')).toBe(false);
    expect(commonValidators.email('test@gmail.c')).toBe(false);
    expect(commonValidators.email(' test@gmail.com ')).toBe(false);

    expect(commonValidators.email('test@gmail.com')).toBe(true);
    expect(commonValidators.email('test@gmail.co')).toBe(true);
  });

  test('equals', () => {
    expect(commonValidators.equals('foo', 'bar')).toBe(false);
    expect(commonValidators.equals('foo bar', 'foobar')).toBe(false);

    expect(commonValidators.equals('foo', 'foo')).toBe(true);
    expect(commonValidators.equals('foo bar', 'foo bar')).toBe(true);
  });

  test('isMobilePhone', () => {
    expect(commonValidators.isMobilePhone('07715301969')).toBe(false);
    expect(commonValidators.isMobilePhone('+447715301969')).toBe(true);
    expect(commonValidators.isMobilePhone('+33612345678')).toBe(true);
    expect(commonValidators.isMobilePhone('+39312123456')).toBe(true);
  });
});
