import { isSingleEmoji } from '../src/util/functions';

describe('isSingleEmoji', () => {
  test('"😀" should return true', () => {
    expect(isSingleEmoji('😀')).toBeTruthy();
  });
  test('" 😀 " should return true', () => {
    expect(isSingleEmoji(' 😀 ')).toBeTruthy();
  });
  test('"  😀  " should return true', () => {
    expect(isSingleEmoji('  😀  ')).toBeTruthy();
  });
  test('"😀foo" should return false', () => {
    expect(isSingleEmoji('😀foo')).not.toBeTruthy();
  });
  test('"😀 foo" should return false', () => {
    expect(isSingleEmoji('😀 foo')).not.toBeTruthy();
  });
  test('"😀 foo bar bat" should return false', () => {
    expect(isSingleEmoji('😀 foo bar bat')).not.toBeTruthy();
  });
  test('" 😀 foo bar bat" should return false', () => {
    expect(isSingleEmoji(' 😀 foo bar bat')).not.toBeTruthy();
  });
  test('"   😀 foo bar bat  " should return false', () => {
    expect(isSingleEmoji('   😀 foo bar bat  ')).not.toBeTruthy();
  });
  test('"!!!   😀 123 ! @ £ $ ^ & * ()  " should return false', () => {
    expect(isSingleEmoji('!!!   😀 123 ! @ £ $ ^ & * ()  ')).not.toBeTruthy();
  });
});
