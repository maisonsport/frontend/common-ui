import * as React from 'react';
import NumberControl, {
  NumberControlProps,
} from '../../molecules/NumberControl';
import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import CardHeading from '../../molecules/CardHeading';
import Card from '../../atoms/Card';
import { Touchable } from '../../index';
import { BoxProps } from '../../atoms/Box/types';

type ConfigItemProps = {
  name: string;
  heading: string;
  renderHeadingTooltip?: () => React.ReactElement;
  quantitySelected: number;

  price?: string;

  unavailable?: boolean;
  plusDisabled?: boolean;
  minusDisabled?: boolean;

  onUnitChange: (
    name: ConfigItemProps['name'],
    action: NumberControlProps['variant'],
  ) => any;
};

// min index of valid dates - 0 & 1 day prices are ignored so start at 2 days
const min_day_scales = 0;
// max index of valid dates - 0 & 1 day prices are ignored so end at 14 days
const max_day_scales = 12;

type ProductConfigurationCardProps = {
  heading: string;
  hideCardShadow?: boolean;
  overrideCardProps?: BoxProps;

  renderHeadingTooltip?: () => React.ReactElement;
  renderMoreAfterText?: () => string;
  renderMoreAfterTextIcon?: () => React.ReactElement;
  renderUnavailableText: () => React.ReactElement;

  accruedTotal?: string;

  configurationItems: ConfigItemProps[];

  bookingTotalDays: number;

  isMobileViewport?: boolean;
};
const ProductConfigurationCard = (props: ProductConfigurationCardProps) => {
  let totalItemsSelected: number[] = [];
  props.configurationItems.forEach((cI, i) => {
    if (!!cI.quantitySelected) totalItemsSelected.push(i);
  });

  totalItemsSelected = totalItemsSelected.sort((a, b) => a - b);
  const dayScalesInView = React.useRef<[number, number]>([
    totalItemsSelected[0] ||
      // 0 & 1 day prices are ignored, so -3 becomes "- 1 day"
      Math.max(props.bookingTotalDays - 3, min_day_scales),
    totalItemsSelected[totalItemsSelected.length - 1] ||
      // 0 & 1 day prices are ignored, so -1 becomes "+ 1 day"
      Math.min(props.bookingTotalDays - 1, max_day_scales),
  ]).current;

  const [showAllDays, setShowAllDays] = React.useState<boolean>(
    !props.renderMoreAfterText,
  );

  const isWithinDayScaleInView = React.useCallback(
    (dayScale: number, allDays: boolean) => {
      return allDays
        ? dayScale >= min_day_scales && dayScale <= max_day_scales
        : dayScale >= dayScalesInView[0] && dayScale <= dayScalesInView[1];
    },
    [dayScalesInView],
  );

  return (
    <Card
      shadow={
        !!props.configurationItems.find((item) => !!item.quantitySelected) &&
        !props.hideCardShadow
      }
      {...(props.overrideCardProps || {})}
    >
      <Box
        flexDirection={'row'}
        justifyContent={'space-between'}
        alignItems={'center'}
      >
        <CardHeading
          text={props.heading}
          renderTooltip={props.renderHeadingTooltip}
          fontSize={2}
        />

        <Box>
          {!!props.accruedTotal && (
            <Text fontSize={2} fontWeight={5} color={'primary'} padding={0}>
              {props.accruedTotal}
            </Text>
          )}
        </Box>
      </Box>

      {props.configurationItems.map((item, i) => {
        if (isWithinDayScaleInView(i, showAllDays)) {
          return (
            <Box
              key={item.heading}
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              marginBottom={2}
            >
              <Box
                flexDirection={'row'}
                flexGrow={1}
                justifyContent={'space-between'}
                marginRight={4}
              >
                <Box
                  flexDirection={'row'}
                  alignItems={'center'}
                  marginRight={2}
                  paddingLeft={1}
                  opacity={item.unavailable ? 0.3 : 1}
                >
                  <Text fontSize={1} padding={0}>
                    {item.heading}{' '}
                    {!!(item.unavailable && props.renderUnavailableText)
                      ? props.renderUnavailableText()
                      : ''}
                  </Text>
                  {!!item.renderHeadingTooltip && item.renderHeadingTooltip()}
                </Box>

                {!props.isMobileViewport &&
                  !!(item.price && !item.unavailable) && (
                    <Text fontSize={1} textAlign={'right'} padding={0}>
                      {item.price}
                    </Text>
                  )}
              </Box>

              <Box>
                {props.isMobileViewport && !!(item.price && !item.unavailable) && (
                  <Text
                    fontSize={1}
                    textAlign={'center'}
                    padding={0}
                    marginBottom={2}
                  >
                    {item.price}
                  </Text>
                )}
                <Box
                  flexDirection={'row'}
                  alignItems={'center'}
                  justifyContent={'center'}
                >
                  <NumberControl
                    onPress={() => item.onUnitChange(item.name, 'minus')}
                    variant={'minus'}
                    disabled={
                      !item.quantitySelected ||
                      !!item.unavailable ||
                      item.minusDisabled
                    }
                  />
                  <Text marginLeft={1} marginRight={1}>
                    {item.unavailable ? '-' : item.quantitySelected}
                  </Text>
                  <NumberControl
                    onPress={() => item.onUnitChange(item.name, 'add')}
                    variant={'add'}
                    disabled={!!item.unavailable || item.plusDisabled}
                  />
                </Box>
              </Box>
            </Box>
          );
        }
        return null;
      })}
      {!showAllDays && props.renderMoreAfterText && (
        <Touchable
          marginTop={3}
          padding={1}
          paddingX={2}
          borderRadius={5}
          backgroundColor={'primary'}
          onPress={() => setShowAllDays(true)}
          flexDirection={'row'}
          justifyContent={'space-between'}
          alignItems={'center'}
          style={{ maxWidth: 'fit-content' }}
        >
          <Text
            color={'white'}
            marginRight={!!props.renderMoreAfterTextIcon ? 2 : 0}
            fontSize={0}
          >
            {props.renderMoreAfterText()}
          </Text>
          {!!props.renderMoreAfterTextIcon && props.renderMoreAfterTextIcon()}
        </Touchable>
      )}
    </Card>
  );
};

export default ProductConfigurationCard;
