import React, { ReactElement } from 'react';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Button from '../../molecules/Button';

import { BookingSummary } from '../../types/booking';
import { UserType } from '../../types/user';
import { TranslatorFunc } from '../../types/global';

type HiddenElements = 'heading' | 'subheading';
export interface Props {
  booking: Pick<
    BookingSummary,
    'cancellationFee' | 'cancellationRefund' | 'cancellationPolicy' | 'currency'
  >;
  onClickedContinue: () => void;
  onClickedClose: () => void;
  t: TranslatorFunc;
  userType: UserType;

  hide?: HiddenElements[];
}

export const closeButtonTestId = 'close-button-test-id';
export const confirmButtonTestId = 'confirm-button-test-id';

export const instructorBookingPromptDescriptionText =
  'text.general.cancel_booking_prompt_description_instructor';
export const customerBookingPromptDescriptionText =
  'text.general.cancel_booking_prompt_refund';

export default function CancelBookingPrompt({
  booking,
  onClickedContinue,
  onClickedClose,
  t,
  userType,

  hide = [],
}: Props): ReactElement {
  const isInstructor = userType === UserType['INSTRUCTOR'];
  return (
    <Box alignItems="center" flexGrow={1} padding={3}>
      {hide?.includes('heading') ? null : (
        <Text variant="h3" textAlign="center" marginBottom={3}>
          {t('text.headings.cancel_booking_prompt_title')}
        </Text>
      )}

      {hide?.includes('subheading') ? null : (
        <Text textAlign="center" fontSize={2}>
          {isInstructor
            ? t(instructorBookingPromptDescriptionText)
            : t(customerBookingPromptDescriptionText, {
                cancellationPolicy: t(
                  'cancellation_policy.' +
                    booking.cancellationPolicy.toLowerCase(),
                ),
                amount: (booking.cancellationRefund / -100).toFixed(2),
                currency: booking.currency,
              })}
        </Text>
      )}

      <Box
        flexDirection="row"
        justifyContent="space-around"
        marginTop={3}
        marginBottom={3}
        width="100%"
        flex={1}
      >
        <Box flex={0.45}>
          <Button
            testID={closeButtonTestId}
            noWrapTheme
            onPress={onClickedClose}
            text={t('text.general.keep_booking')}
            variant="primary"
          />
        </Box>

        <Box flex={0.45}>
          <Button
            testID={confirmButtonTestId}
            noWrapTheme
            onPress={onClickedContinue}
            text={t('text.general.cancel_booking')}
            variant="attention"
          />
        </Box>
      </Box>
    </Box>
  );
}
