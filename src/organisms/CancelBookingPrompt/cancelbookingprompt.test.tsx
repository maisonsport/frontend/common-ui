import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import { CancellationPolicy } from '../../types/booking';
import { Currency } from '../../types/currency';
import { UserType } from '../../types/user';

import CancelBookingPrompt, {
    Props as CancelBookingPromptProps,

    closeButtonTestId,
    confirmButtonTestId,
    customerBookingPromptDescriptionText,
    instructorBookingPromptDescriptionText,
} from '.';

const defaultProps: CancelBookingPromptProps = {
    booking: {
        cancellationRefund: 10,
        cancellationPolicy: CancellationPolicy['FLEXIBLE'],
        currency: Currency['EUR'],
        cancellationFee: 100,
    },
    userType: UserType['INSTRUCTOR'],
    t: (s) => s,
    onClickedClose: () => {},
    onClickedContinue: () => {},
};

describe('CancelBookingPrompt', () => {
    test('@prop: onClickClose', () => {
        const spy = jest.fn();
        const { getByTestId } = render(<CancelBookingPrompt {...defaultProps} onClickedClose={spy} />);

        const button = getByTestId(closeButtonTestId);

        fireEvent.press(button);

        expect(spy).toHaveBeenCalledTimes(1);
    });
    test('@prop: onClickContinue', () => {
        const spy = jest.fn();
        const { getByTestId } = render(<CancelBookingPrompt {...defaultProps} onClickedContinue={spy} />);

        const button = getByTestId(confirmButtonTestId);

        fireEvent.press(button);

        expect(spy).toHaveBeenCalledTimes(1);
    });
    describe('@prop: userType', () => {
        test(UserType['INSTRUCTOR'], () => {
            const { getByText } = render(<CancelBookingPrompt {...defaultProps} />);

            expect(() => getByText(customerBookingPromptDescriptionText)).toThrow();
            expect(getByText(instructorBookingPromptDescriptionText)).toBeTruthy();
        });
        test(UserType['CUSTOMER'], () => {
            const { getByText } = render(<CancelBookingPrompt {...defaultProps} userType={UserType['CUSTOMER']} />);

            expect(() => getByText(instructorBookingPromptDescriptionText)).toThrow();
            expect(getByText(customerBookingPromptDescriptionText)).toBeTruthy();
        });
    });
});
