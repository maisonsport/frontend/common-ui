import * as React from 'react';
import Box from '../../atoms/Box';
import CardHeading from '../../molecules/CardHeading';

type ProductConfigurationSectionProps = {
  heading?: string;
  renderHeadingTooltip?: () => React.ReactElement;

  isMobile?: boolean;

  noHeading?: boolean;
  children: any;
};
const ProductConfigurationSection = (
  props: ProductConfigurationSectionProps,
) => {
  return (
    <Box padding={props.isMobile ? 0 : 3} flex={1}>
      {!props.noHeading && (
        <CardHeading
          text={props.heading || 'XXX'}
          opacity={props.heading ? 1 : 0}
          renderTooltip={props.renderHeadingTooltip}
        />
      )}

      {props.children}
    </Box>
  );
};

export default ProductConfigurationSection;
