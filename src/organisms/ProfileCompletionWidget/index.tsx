import React, { ReactElement } from 'react';
import { Image } from 'react-native';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';
import Theme from '../../theme/index.js';

import { CompletionTask, ProfileCompletion, TaskLink } from '../../types/instructor';

interface CompletionTaskWithoutLink extends Omit<CompletionTask, 'link'> {}
interface ProfileCompletionWithoutTaskLink extends Omit<ProfileCompletion, 'tasks'> {
  tasks: CompletionTaskWithoutLink[];
}
export interface ProfileCompletionWidgetPropsBase {
  completionData: ProfileCompletion;
  EditAnchor: (props: any) => ReactElement;
}

interface PropsWithLink extends ProfileCompletionWidgetPropsBase {
  onLinkPress: (link: TaskLink) => void;
}
interface PropsWithoutLink extends ProfileCompletionWidgetPropsBase {
  completionData: ProfileCompletionWithoutTaskLink;
}

type Props = PropsWithLink & PropsWithoutLink;

const CompleteIcon = (props: any) => (
  <Image source={require('../../assets/images/complete.png')} style={{ width: 20, height: 20, tintColor: Theme.colors.green }} {...props} />
);
const IncompleteIcon = (props: any) => (
  <Image source={require('../../assets/images/incomplete.png')} style={{ width: 20, height: 20, tintColor: Theme.colors.grey }} {...props} />
);

const ProfileCompletionWidget = ({
  EditAnchor,
  completionData,
  onLinkPress = () => null
}: Props): ReactElement => {
  if (!completionData) return <></>;

  return (
    <Box>
      {completionData.tasks.map((task) => {
        const Icon = task.completed ? CompleteIcon : IncompleteIcon;

        let TextComponent = (props: any) => <Text textAlign="left" marginLeft={3} {...props} />;
        const TaskLinkPress = (props: any) => <Touchable onPress={() => onLinkPress(task.link!)} {...props} />
        if (task.link && !task.link.text) {
          const T = TextComponent;
          TextComponent = () => (
            <TaskLinkPress>
              <T variant="link">{task.name}</T>
            </TaskLinkPress>
          );
        }

        return (
          <Box
            key={task.name}
            flexDirection="row"
            alignItems="center"
            paddingTop={2}
            paddingBottom={2}
          >
            <Icon />
            <TextComponent>{task.name}</TextComponent>
            {task.link?.text && (
              <TaskLinkPress>
                <TextComponent variant="link">{task.link.text}</TextComponent>
              </TaskLinkPress>
            )}
          </Box>
        );
      })}

      {!!EditAnchor && <EditAnchor />}
    </Box>
  );
}

export default ProfileCompletionWidget;
