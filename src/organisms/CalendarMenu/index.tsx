import React from 'react';

import Box from '../../atoms/Box';
import Theme from '../../theme/index';
import { CalendarSyncStatus } from '../../types/calendar';

import MenuAction, { DEFAULT_ACTION_SIZE } from '../../molecules/CalendarMenuAction';

import BackDrop from './BackDrop';

import { TranslatorFunc } from '../../types/global';

export type MenuActions = 'settings' | 'new' | 'sync';
interface Props {
    visible: boolean;
    hide?: MenuActions[];
    onActionPress: (action: MenuActions) => void;
    onToggleMenuVisible: (visible: boolean) => void;

    toggleActionVariant?: 'dark' | 'light';
    withBackdrop?: boolean;
    bottomOffset?: string|number;
    rightOffset?: string|number;

    t: TranslatorFunc;
    calendarSyncStatus: CalendarSyncStatus;
}

const Menu = (props: Props) => {
    const { hide = [] } = props;
    function _show() {
        props.onToggleMenuVisible(true);
    }
    function _hide() {
        props.onToggleMenuVisible(false);
    }

    const menuItemSize = DEFAULT_ACTION_SIZE * 0.85;
    const menuItemMarginRight = (DEFAULT_ACTION_SIZE * 0.15) / 2;

    return (
        <>
            {props.withBackdrop && <BackDrop onPress={_hide} visible={props.visible} />}
            <Box
                style={{
                    position: 'absolute',
                    bottom: (props.bottomOffset || Theme.space[3]),
                    right: (props.rightOffset || Theme.space[3])
                }}>
                {props.visible ? (
                    <>
                        {!hide.includes('sync') && <MenuAction.Sync
                            variant="dark"
                            size={menuItemSize}
                            t={props.t}
                            onPress={() => props.onActionPress('sync')}
                            marginBottom={3}
                            marginRight={menuItemMarginRight}
                            calendarSyncStatus={props.calendarSyncStatus} />}
                        {!hide.includes('settings') && <MenuAction.Settings textBackdropColor="primary" size={menuItemSize} t={props.t} onPress={() => props.onActionPress('settings')} marginBottom={3} marginRight={menuItemMarginRight} />}
                        {!hide.includes('new') && <MenuAction.Calendar textBackdropColor="primary" t={props.t} size={menuItemSize} onPress={() => props.onActionPress('new')} marginBottom={3} marginRight={menuItemMarginRight} />}
                        <MenuAction.Close onPress={_hide} />
                    </>
                ) : (
                    <MenuAction.Plus variant={props.toggleActionVariant} onPress={_show} />
                )}
            </Box>
        </>
    )
}

export default Menu;
