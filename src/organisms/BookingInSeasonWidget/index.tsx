import React, { ReactElement } from 'react';

import ActivityIndicator from '../../atoms/ActivityIndicator';

import { Currency } from '../../types/currency';

interface Props {
  currency: Currency;
  bookingValue: number;
  currencyFormatter: (amount: number, options: Intl.NumberFormatOptions) => string;
  render: (value: string) => ReactElement;
}

const BookingInSeasonWidget = ({
  currency,
  bookingValue,
  currencyFormatter,
  render,
}: Props) => {

  if(typeof bookingValue === 'undefined') {
    return (
      <ActivityIndicator size="large" />
    );
  }

  return render(currencyFormatter(
    bookingValue,
    { currency },
  ));
}

export default BookingInSeasonWidget;
