import * as React from "react";
import Card from "../../atoms/Card";
import Box from "../../atoms/Box";
import CardHeading from "../../molecules/CardHeading";
import Button from "../../molecules/Button";
import Theme from "../../theme";

type BillProps = {
	heading: string;
	children: any;
	formattedTotal: string;
	ctaText: string;
	ctaOnPress: () => any;
};
const Bill = (props: BillProps) => {
	return (
		<Card shadow padding={0}>
			<Box
				padding={3}
				justifyContent={'center'}
				alignItems={'center'}
				backgroundColor={Theme.colors.lightBlue}
				borderRadius={5}
			>
				<CardHeading text={props.heading} marginBottom={0} />
			</Box>

			<Card paddingBottom={0}>
				{props.children}

				{!!props.formattedTotal && (
					<Box
						marginTop={3}
						flexDirection={'row'}
						justifyContent={'space-between'}
					>
						<CardHeading text={'Total'} />
						<CardHeading text={props.formattedTotal} />
					</Box>
				)}

				{!!props.ctaText && (
					<Box>
						<Button text={props.ctaText} onPress={props.ctaOnPress} />
					</Box>
				)}
			</Card>
		</Card>
	);
};

export default Bill;
