import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import LessonSummaryHeader, {
  lessonSummaryHeaderContainerTestID,
  chevronTestID,
} from './index.tsx';

describe('LessonSummaryHeader', () => {
  test('@prop: title', () => {
    const titleText = 'This is a title!';
    const { getByText } = render(<LessonSummaryHeader title={titleText} />);

    expect(getByText(titleText)).toBeTruthy();
  });
  test('@prop: subtitle', () => {
    const subtitleText = 'This is a subtitle!';
    const { getByText } = render(<LessonSummaryHeader subtitle={subtitleText} />);

    expect(getByText(subtitleText)).toBeTruthy();
  });
  test('@prop: onPress', () => {
    const onPress = jest.fn().mockName('onPress');
    const { getByTestId } = render(<LessonSummaryHeader {...{ onPress }} />);

    expect(getByTestId(chevronTestID)).toBeTruthy();

    fireEvent.press(getByTestId(lessonSummaryHeaderContainerTestID));

    expect(onPress).toHaveBeenCalled();
  });
  test('@prop: onPress [undefined]', () => {
    const { getByTestId } = render(<LessonSummaryHeader />);

    expect(() => getByTestId(chevronTestID)).toThrow();
  });
});
