import { ReactElement } from 'react';

import formatDuration from 'date-fns/formatDuration';
import { enGB, fr, it, de } from 'date-fns/locale';

import Theme from '../../theme/index.js';

import { Profile } from '../../types/instructor';

interface RenderResponseTimeProps {
  responseTimeString: string;
  color: string;
}
interface Props {
  responseTime: Profile['responseTime'];
  i18n: any;
  renderResponseTime: (props: RenderResponseTimeProps) => ReactElement;
}

const ResponseTimeWidget = ({
  responseTime = 0,
  i18n,
  renderResponseTime,
}: Props): ReactElement => {
  let color: string = Theme.colors.green;
  if (responseTime > 240) color = Theme.colors.amber;
  if (responseTime > 1440) color = Theme.colors.red;

  let responseTimeString: string|null = null;
  if (responseTime === null) {
    responseTimeString =  'widget.response-time.insufficient';
    color = 'textGrey';
  } else {
    responseTimeString = formatDuration({
      hours: Math.floor(responseTime / 60),
      minutes: responseTime % 60
    }, {
      locale: {
        en: enGB, fr, it, de
      }[i18n.language]
    });
  }

  return renderResponseTime({ responseTimeString, color });
}

export default ResponseTimeWidget;
