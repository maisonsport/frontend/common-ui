import * as React from "react";
import {TabProps} from "../../molecules/Tab";
import Theme from "../../theme";
import Box from "../../atoms/Box";

type TabConfig = {
	[tabName: string]: (
		props: Pick<TabProps, 'isSelected'> & { key: string },
	) => React.ReactElement;
};
type ContentsConfig = {
	[tabName in keyof TabConfig]: React.ReactElement;
};
export type TabBarProps = {
	tabs: TabConfig;
	contents: ContentsConfig;

	selectedTab: keyof TabConfig;
};
const TabBar = (props: TabBarProps) => {
	return (
		<Box>
			<Box flexDirection={'row'} justifyContent={'space-evenly'}>
				{Object.entries(props.tabs).map(([tabName, render]) => {
					return render({
						isSelected: props.selectedTab === tabName,
						key: `tab_${tabName}`,
					});
				})}
			</Box>
			<Box
				borderWidth={0.5}
				borderTopWidth={0}
				borderColor={'primary'}
				padding={3}
				backgroundColor={Theme.colors.lightBlue}
				width={1}
				borderBottomLeftRadius={5}
				borderBottomRightRadius={5}
			>
				{props.contents[props.selectedTab] || null}
			</Box>
		</Box>
	);
};

export default TabBar;
