import { getFormattedTimeSent } from '../dateTime';

const MockDate = require('mockdate');

describe('getFormattedTime', () => {
  beforeEach(() => {
    MockDate.set(new Date(2021, 1, 15, 13, 0, 0));
  });

  afterEach(() => {
    MockDate.reset();
  });

  const timeSentValues = [
    ['2021-02-14 12:00:00', 'text.general.time_yesterday'],
    ['2021-02-15 12:59:00', 'text.general.time_just_now'],
    ['2021-02-15 12:52:00', '12:52'],
    ['2021-02-13 11:51:00', '13/02/21'],
  ];

  test.each(timeSentValues)('Returns the expected time sent format', (timeSent, expectedValue) => {
    expect(getFormattedTimeSent(timeSent, (key) => key)).toBe(expectedValue);
  });
});
