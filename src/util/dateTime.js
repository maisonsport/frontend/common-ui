import differenceInMinutes from 'date-fns/differenceInMinutes';
import isYesterday from 'date-fns/isYesterday';
import isToday from 'date-fns/isToday';
import format from 'date-fns/format';

// eslint-disable-next-line import/prefer-default-export
export function getFormattedTimeSent(timeSent, t, withTime = false) {
  const justNowThreshold = 2;
  const currentDateTime = new Date();
  const timeSentDate = new Date(timeSent.replace(' ', 'T')); // convert to an accepted format for Date construction

  if (differenceInMinutes(currentDateTime, timeSentDate) > 0
        && differenceInMinutes(currentDateTime, timeSentDate) <= justNowThreshold) {
    return t('text.general.time_just_now');
  }

  if (isYesterday(timeSentDate)) {
    return t('text.general.time_yesterday');
  }

  if (isToday(timeSentDate)) {
    return format(timeSentDate, 'HH:mm');
  }

  return withTime
    ? format(timeSentDate, 'dd/MM/yy HH:mm')
    : format(timeSentDate, 'dd/MM/yy');
}
