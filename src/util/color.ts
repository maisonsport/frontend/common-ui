import isDarkColor from 'is-dark-color';

import Theme from '../theme';

interface ColorOptions {
    light: string
    dark: string
}

const defaultColorOptions: ColorOptions =  {
    light: Theme.colors.white,
    dark: Theme.colors.textGrey,
}

/** https://www.npmjs.com/package/is-dark-color */
const isDarkColorOpts = {
    override: {
        [Theme.colors.textGrey]: true,
        [Theme.colors.amber]: true,
        [Theme.colors.green]: true,
        [Theme.colors.lightRed]: true,
        [Theme.colors.primary]: true,
    }
}

export function getCorrectTextColorForBackground(bgColor: string, colorOpts: ColorOptions = defaultColorOptions) {
    const lightColor = colorOpts.light;
    const darkColor = colorOpts.dark;

    return isDarkColor(bgColor || Theme.colors.primary, isDarkColorOpts) ? lightColor : darkColor;
}