import { BUTTON_HEIGHT, TEXT_INPUT_HEIGHT } from '../constants/form';

const colors = {
  amber: '#DBA24A',
  background: '#fff',
  grey: '#D5D5D5',
  placeholderGrey: '#9daab5',
  lightGrey: '#f4f4f4',
  darkGrey: '#3d464d',
  textGrey: '#3d464d',
  notification: '#F12F2E',
  primary: '#3592fb',
  darkPrimary: '#205897',
  lightPrimary: '#86befd',
  dustyBlue: '#5a6e84',
  lightBlack: '#222',
  black: '#1d1d1d',
  white: '#fff',
  navy: '#004175',
  red: '#F12F2E',
  darkRed: '#911c1c',
  lightRed: '#f78282',
  green: '#2ecc71',
  darkGreen: '#1c7a44',
  lightGreen: '#82e0aa',
  convoPink: '#f1f0f4',
  convoGrey: '#f4f3f1',
  convoBlue: '#e8ecef',
  lightBlue: '#B7D9FF',
};

const space = [0, 4, 8, 16, 32, 64, 128, 256, 512];

const borderWidths: number[] | any = [0, 4, 8, 16];
borderWidths.thin = 1;
borderWidths.medium = 2;

const fontSizes = [12, 14, 16, 20, 24, 32];

const sizes: string[] | any = [
  'auto',
  `${100}%`,
  `${100 / 2}%`,
  `${100 / 3}%`,
  `${100 / 4}%`,
  `${100 / 5}%`,
  `${100 / 6}%`,
  `${100 / 7}%`,
  `${100 / 8}%`,
  `${100 / 9}%`,
  `${100 / 10}%`,
];
sizes.textInputHeight = `${TEXT_INPUT_HEIGHT}px`;
sizes.textInputHeight_raw = TEXT_INPUT_HEIGHT;
sizes.buttonHeight = `${BUTTON_HEIGHT}px`;
sizes.buttonHeight_raw = BUTTON_HEIGHT;
sizes.messageBookingHeaderHeight = '100px';
sizes.messageBookingHeaderHeight_raw = 100;

const fontWeights: string[] | any = [
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900',
];
const fonts = [
  'Apercu-Light',
  'Apercu-Regular',
  'Apercu',
  'Apercu-Medium',
  'Apercu-Bold',
];
const indicator = {
  color: colors.black,
  size: 'small',
};
export const activityIndicator = {
  primary: {
    ...indicator,
    color: colors.primary,
  },
  secondary: {
    ...indicator,
    color: colors.white,
  },
  tertiary: {
    ...indicator,
    color: colors.darkGrey,
  },
  attention: {
    ...indicator,
    color: colors.red,
  },
  success: {
    ...indicator,
    color: colors.green,
  },
  button_primary: {
    ...indicator,
    color: colors.white,
  },
  button_secondary: {
    ...indicator,
    color: colors.primary,
  },
  button_tertiary: {
    ...indicator,
    color: colors.white,
  },
  button_attention: {
    ...indicator,
    color: colors.white,
  },
  button_success: {
    ...indicator,
    color: colors.white,
  },
  button_disabled: {
    ...indicator,
    color: colors.grey,
  },
} as const;

const baseText = {
  fontFamily: fonts[2],
};
const boldText = {
  fontFamily: fonts[4],
};
const buttonText = {
  fontSize: fontSizes[2],
  textAlign: 'center',
};
export const text = {
  h1: {
    ...baseText,
    ...boldText,
    color: colors.black,
    fontSize: fontSizes[5],
    fontWeight: fontWeights[5],
    marginVertical: space[3],
  },
  h2: {
    ...baseText,
    ...boldText,
    color: colors.black,
    fontSize: fontSizes[4],
    fontWeight: fontWeights[5],
    marginVertical: space[3],
  },
  h3: {
    ...baseText,
    ...boldText,
    color: colors.black,
    fontSize: fontSizes[3],
    fontWeight: fontWeights[5],
    padding: space[2],
  },
  h4: {
    ...baseText,
    ...boldText,
    color: colors.black,
    fontSize: fontSizes[2],
    fontWeight: fontWeights[5],
    padding: space[1],
  },
  p: {
    ...baseText,
    color: colors.black,
    fontWeight: fontWeights[2],
    fontSize: fontSizes[1],
    padding: space[1],
  },
  link: {
    ...baseText,
    color: colors.primary,
    fontWeight: fontWeights[2],
    fontSize: fontSizes[1],
    padding: space[1],
    textDecorationLine: 'underline',
  },
  textInputLabel: {
    ...baseText,
    color: colors.black,
    fontWeight: fontWeights.bold,
    fontSize: fontSizes[3],
  },
  button_primary: {
    ...baseText,
    ...buttonText,
    color: colors.white,
  },
  button_secondary: {
    ...baseText,
    ...buttonText,
    color: colors.primary,
  },
  button_tertiary: {
    ...baseText,
    ...buttonText,
    color: colors.white,
  },
  button_attention: {
    ...baseText,
    ...buttonText,
    color: colors.white,
  },
  button_success: {
    ...baseText,
    ...buttonText,
    color: colors.white,
  },
  button_disabled: {
    ...baseText,
    ...buttonText,
    color: colors.darkGrey,
  },
} as const;

const buttonContainer = {
  paddingLeft: space[3],
  paddingRight: space[3],
  borderRadius: 4,
  width: sizes[1],
  borderWidth: borderWidths.thin,
  justifyContent: 'center',
};
export const buttons = {
  primary: {
    ...buttonContainer,
    backgroundColor: colors.primary,
    borderColor: colors.primary,
  },
  secondary: {
    ...buttonContainer,
    backgroundColor: colors.white,
    borderColor: colors.primary,
  },
  tertiary: {
    ...buttonContainer,
    backgroundColor: colors.darkGrey,
    borderColor: colors.darkGrey,
  },
  attention: {
    ...buttonContainer,
    backgroundColor: colors.red,
    borderColor: colors.red,
  },
  success: {
    ...buttonContainer,
    backgroundColor: colors.green,
    borderColor: colors.green,
  },
  disabled: {
    ...buttonContainer,
    backgroundColor: colors.lightGrey,
    borderColor: colors.grey,
  },
} as const;

const variants = {
  activityIndicator,
  buttons,
  text,
};

const Theme = {
  colors,
  space: {
    ...space,
    small: space[1],
    medium: space[2],
    large: space[3],
    xlarge: space[4],
  },
  sizes,
  fonts,
  fontSizes,
  borderWidths,
  fontWeights: {
    ...fontWeights,
    medium: fontWeights[4],
    bold: 'bold',
    black: fontWeights[8],
  },
  globals: {
    boxShadow: {
      shadowColor: colors.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
    },
  },

  ...variants,
};
export default Theme;
