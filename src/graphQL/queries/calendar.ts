import { Day, Slot } from "../../types/calendar";

interface SlotWithDateStrings extends Omit<Slot, 'start'|'end'> {
    start: string;
    end: string;
}
interface DayWithDateStringSlots extends Omit<Day, 'slots'> {
    slots: SlotWithDateStrings[];
}
export type MONTH_VIEW_DAY_RESPONSE_DAY = DayWithDateStringSlots;

export const MONTH_VIEW_DAY = `
    day
    isInSeason
    rule {
        id
        name
        color
        times
        dayInOrder
    }
    pricing {
        perHourMorning
        perHourAfternoon
        perDay
        currency
    }
    bookableSlots {
        start
        end
        isFullDay
        isAfternoon
        isAvailable
    }
    slots {
        id
        start
        end
        isFullDay
        isRecurring
        recurringEndDate
        recurringInterval
        type
        customer {
            id
            email
            displayName
            firstName
            lastName
            phone
            avatar{
                url
                name
            }
        }
        booking {
            id
            referenceNumber
            isProtected
            status {
                state
            }
        }
        title
        link
        extService
        color
    }
    resorts{
        name
        slug
        id
        schoolRates{
            DAY
            HOUR
        }
        countryCode
    }
`;
