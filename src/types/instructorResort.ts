import { ResortSchoolRates } from "./resortSchoolRates";

export interface InstructorResort {
    name: string;
    slug: string;
    id: number;
    schoolRates?: ResortSchoolRates;
    popular: boolean;
    countryCode: string;
    fromDate: Date;
    toDate: Date;
    isMainResort: boolean;
}