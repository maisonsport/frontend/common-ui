import { DateTime } from '../global';
import { DayPricing } from '../calendar';

export interface BookableSlotInput {
    start: DateTime;
    end: DateTime;
    isAvailable: boolean;
    isFullDay: boolean;
}

export interface CalendarDayInput {
    bookableSlots?: BookableSlotInput[];
    pricing?: DayPricing;
    resortIds?: number[];
    resetBookableSlots?: boolean;
    resetPricing?: boolean;
    resetResorts?: boolean;
    resetRule?: boolean;
}