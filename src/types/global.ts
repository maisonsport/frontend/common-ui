export type TranslatorFunc = (str: string, interpolateObj?: { [key: string]: string }) => string;
export type I18nLocale = string;

/**
 * A date string formatted to yyyy-mm-dd
 */
export type DateTime = string;

export interface NoWrapTheme { noWrapTheme?: boolean }