export interface Image {
  id: string;
  name: string;
  kind: string;
  mimeType: string;
  url: string;
  size?: number;
  sizes?: string[];
}
