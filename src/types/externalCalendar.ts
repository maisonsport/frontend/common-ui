enum ExternalCalendarStatus {
    CONNECTED = "CONNECTED",
    NOT_CONNECTED = "NOT_CONNECTED",
    REQUIRES_AUTHENTICATION = "REQUIRES_AUTHENTICATION",
}

interface Calendar {
    id: string,
    name: string
}

export interface ExternalCalendar {
    status: ExternalCalendarStatus,
    provider: string,
    availableCalendars: [Calendar],
    selectedCalendars: [Calendar]
}
