export enum Currency {
  EUR = "EUR",
  CHF = "CHF",
  GBP = "GBP",
}
