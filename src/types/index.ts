import * as BookingTypes from './booking';
import * as CalendarTypes from './calendar';
import * as GlobalTypes from './global';
import * as Instructor from './instructor';
import * as User from './user';

import * as CalendarMutationInputs from './mutations/calendar';

import { Image } from './image';
import { Currency } from './currency';
import { InstructorResort } from './instructorResort';
import { PaymentProviders } from './paymentProviders';
import { ResortSchoolRates } from './resortSchoolRates';
import { MessageThread } from './thread';

export {
    BookingTypes,
    CalendarTypes,
    GlobalTypes,
    Instructor,
    User,

    CalendarMutationInputs,

    Image,
    Currency,
    InstructorResort,
    PaymentProviders,
    ResortSchoolRates,
    MessageThread,
};
