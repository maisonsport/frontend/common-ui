export type Payout = {
  amount: number
  currencyCode: string
  reference: string
  transactionUuid: string
  externalId: string
  state: string
  createdAt: Date
}
