import { I18nLocale } from "../types/global";

interface Opts {
    locale: I18nLocale;
}
interface FormatOptions extends Intl.NumberFormatOptions {
    isNotPennies?: boolean;
}
interface UseCurrencyFormatterReturn {
    format: (amount: number, options: FormatOptions) => string;
    penniesToPounds: (pennies: number) => number;
}

function useCurrencyFormatter(opts: Opts): UseCurrencyFormatterReturn {
    const LOCALE = opts.locale || 'en';

    function penniesToPounds(pennies: number) {
        return pennies / 100;
    }

    function format(amount: number, options: FormatOptions = {}): string {
        const {
            isNotPennies,
            ...opts
        } = options;

        const amt = isNotPennies ? amount : penniesToPounds(amount);

        return new Intl.NumberFormat(LOCALE, { style: 'currency', ...opts }).format(amt);
    }

    return {
        format,
        penniesToPounds,
    }

}

export default useCurrencyFormatter;
