/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/destructuring-assignment */
import React, {
  forwardRef,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  FlatList,
  FlatListProps,
  Image,
  Keyboard,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  space,
  layout,
  flexbox,
} from 'styled-system';
import composeRefs from '@seznam/compose-react-refs';

import Theme from '../../theme/index';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';

import TextInput from '../../molecules/TextInput';
import MessageBubble from '../../molecules/MessageBubble';
import MessageSendButton from '../../molecules/MessageSendButton';
import { getFormattedTimeSent } from '../../util/dateTime';
import { TranslatorFunc } from '../../types/global';

const isIOS = Platform.OS === 'ios';

const StyledFlatList = styled.FlatList`
  ${layout}
  ${space}
  ${flexbox}
`;
StyledFlatList.defaultProps = {
  position: 'absolute',
};

interface Message {
  id: string | number
  from?: {
    __typename: string
    id: string | number
    displayName: string
  }
  content: string
  createdAt: string
  readAt: string
  renderCustom?: (data: Message & Pick<Props, 'userID'|'recipientID'>) => any
}
interface Props {
  inputPlaceholder: string
  inputValue?: string
  inputBusy?: boolean
  inputHeight?: string | number
  inputBottomOffset?: number
  refreshing?: boolean
  onInputChangeText: (value: string) => any
  onInputSubmit: () => any
  onMessagePress?: (args: Message & Pick<Props, 'userID' | 'recipientID'>) => any
  onMessageLongPress?: (args: Message & Pick<Props, 'userID' | 'recipientID'>) => any
  onRefresh?: () => any

  messages?: Message[]
  unreadMessageIndicatorText?: string
  userID: string | number
  recipientID: string | number
  t: TranslatorFunc

  renderItem: (props: { item: Message }) => any

  ListHeaderComponent: React.ReactNode
  stickyHeaderIndices?: FlatListProps<'stickyHeaderIndices'>
}

const MessageThread = forwardRef(({
  messages = [],
  userID,
  recipientID,

  inputPlaceholder,
  inputValue = '',
  inputBusy = false,
  inputHeight = Theme.sizes.textInputHeight_raw,
  inputBottomOffset = 0,
  unreadMessageIndicatorText = 'New messages',

  onInputSubmit = () => {},
  onInputChangeText = () => {},
  onMessagePress = () => {},
  onMessageLongPress = () => {},
  renderItem,

  t,

  ListHeaderComponent = null,

  ...props
}: Props, ref) => {
  const [inputHeightState, setInputHeightState] = useState(inputHeight);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [keyboardOpen, setKeyboardOpen] = useState(false);
  const [showUnreadMarker, setShowUnreadMarker] = useState(true);

  const threadRef = useRef<FlatList>();
  const threadTrueHeight = useRef(0);
  const scrollPosition = useRef();
  const previousMessagesLength = useRef(messages.length);

  const androidExtraInputPaddingBottom = (
    isIOS
      ? 0
      : Theme.space[3]
  );
  const finalTextInputContainerHeight = (
    inputHeightState
      + Theme.space[3]
      + androidExtraInputPaddingBottom
      + (showUnreadMarker ? (Theme.space[2] * 4) : 0)
      + inputBottomOffset
  );
  const conversationThreadBottom = (
    finalTextInputContainerHeight
      + keyboardHeight
  );

  function keyboardDidShow(event) {
    const keyboardHeightAfterShow = (
      isIOS
        ? event?.endCoordinates?.height || 0
        : 0
    );

    if (isIOS) setKeyboardOpen(true);

    setKeyboardHeight(keyboardHeightAfterShow);
  }
  function keyboardDidHide() {
    setKeyboardOpen(false);
    setKeyboardHeight(0);
  }

  function handleSubmit() {
    if (!!inputValue !== false) {
      onInputSubmit();
    }
  }

  function scrollToBottom() {
    // eslint-disable-next-line no-undef
    setTimeout(() => {
      if (threadRef?.current?.scrollToEnd) {
        threadRef.current.scrollToEnd();
        setShowUnreadMarker(false);
      }
    }, 500);
  }

  function renderMessage(item) {
    const { content, createdAt, from } = item;
    const position: any = {};
    if (from?.id === userID) position.right = true;
    if (from?.id === recipientID) position.left = true;

    const formattedCreatedAt = getFormattedTimeSent(createdAt, t, true);

    return (
      <MessageBubble
        {...position}
        timeSent={formattedCreatedAt}
        onPress={() => onMessagePress({ ...item, userID, recipientID })}
        onLongPress={() => onMessageLongPress({ ...item, userID, recipientID })}
      >
        {
          from?.id ? content : `${content} \n ${formattedCreatedAt}`
        }
      </MessageBubble>
    );
  }

  useEffect(() => {
    const showListener = Keyboard.addListener('keyboardDidShow', keyboardDidShow);
    const hideListener = Keyboard.addListener('keyboardDidHide', keyboardDidHide);

    return () => {
      showListener?.remove && showListener.remove()
      hideListener?.remove && hideListener.remove()
    };
  }, []);

  useEffect(() => {
    const { current: prevLength } = previousMessagesLength;
    const { current: lastKnownScrollPosition } = scrollPosition || {};
    const { current: threadContentHeight } = threadTrueHeight || {};

    if (prevLength < messages.length) {
      previousMessagesLength.current = messages.length;

      let lastKnown = lastKnownScrollPosition || threadContentHeight;
      if (lastKnown < (threadContentHeight - 800)) {
        setShowUnreadMarker(true);
      } else {
        scrollToBottom();
      }
    }
  }, [messages]);

  useEffect(() => {
    scrollToBottom();
  }, []);

  return (
    <ThemeProvider theme={Theme}>
      <Box
        height={1}
        position="relative"
      >
        <StyledFlatList
          ref={composeRefs(ref, threadRef)}
          top={0}
          bottom={conversationThreadBottom}
          left={0}
          right={0}
          backgroundColor={Theme.colors.background}
          data={messages}
          stickyHeaderIndices={
            (!keyboardOpen && ListHeaderComponent)
              ? [0]
              : props.stickyHeaderIndices || []
          }
          scrollEventThrottle={200}
          keyExtractor={(item) => item.id}
          ListHeaderComponent={keyboardOpen ? null : (ListHeaderComponent || null)}
          renderItem={({ item }) => {
            if (item.renderCustom) {
              return item.renderCustom({ item, userID, recipientID });
            }
            if (renderItem) {
              return renderItem({ item });
            }
            return renderMessage(item);
          }}
          onScroll={(event) => {
            threadTrueHeight.current = event.nativeEvent.contentSize.height;
            scrollPosition.current = event.nativeEvent.contentOffset.y;
          }}
          {...props}
        />

        <Box
          height={finalTextInputContainerHeight}
          position="absolute"
          alignItems="flex-end"
          justifyContent="flex-end"
          bottom={keyboardHeight + androidExtraInputPaddingBottom}
          left={0}
          right={0}
        >
          {showUnreadMarker && (
            <Touchable
              padding={2}
              borderRadius={20}
              borderWidth={0.5}
              borderColor="primary"
              marginBottom={2}
              flexDirection="row"
              alignSelf="center"
              alignItems="center"
              justifyContent="space-around"
              backgroundColor="background"
              onPress={scrollToBottom}
            >
              <Text color="primary" fontSize={0} fontWeight="bold" padding={0} marginRight={2}>{unreadMessageIndicatorText}</Text>
              <Image
                source={require('../../assets/images/chevron.png')}
                style={{
                  tintColor: Theme.colors.primary,
                  width: 10,
                  height: 10,
                  transform: [{ rotate: '90deg' }],
                }}
              />
            </Touchable>
          )}
          <Box
            flexDirection="row"
            justifyContent="space-between"
            alignItems="flex-end"
            paddingLeft={3}
            marginBottom={inputBottomOffset}
          >
            <Box flexGrow={1}>
              <TextInput
                placeholder={inputPlaceholder}
                returnKeyType="send"
                value={inputValue}
                onChangeText={onInputChangeText}
                onContentSizeChange={(event) => {
                  const heightAfterChange = (
                    inputHeight + (event?.nativeEvent?.contentSize?.height || 0)
                  );

                  if (heightAfterChange >= Theme.sizes.textInputHeight_raw * 5) return;

                  setInputHeightState(heightAfterChange);
                }}
                fontSize={1}
                multiline
                numberOfLines={25}
                height={inputHeightState}
                maxHeight={Theme.sizes.textInputHeight_raw * 5}
                marginRight={3}
                width={1}
                iconRight={null}
                backgroundColor={Theme.colors.convoPink}
                style={{
                  backgroundColor: Theme.colors.convoPink,
                  textAlignVertical: 'bottom',
                }}
              />
            </Box>
            <Box height={inputHeight} alignItems="center" justifyContent="center">
              <MessageSendButton
                alignSelf="center"
                onPress={handleSubmit}
                disabled={!!inputValue === false}
                busy={inputBusy}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </ThemeProvider>
  );
});

export default MessageThread;
