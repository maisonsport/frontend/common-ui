/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { ScrollViewProps } from 'react-native';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  flexbox,
  opacity,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme/index';

import { ScrollableProps } from './types';

export const testScrollableID = 'scrollable';

const StyledScrollView = styled.ScrollView.attrs(A11y)`
  ${flexbox}
  ${layout}
  ${borders}
  ${space}
  ${color}
  ${opacity}
`;

function Scrollable({
  noWrapTheme = false,
  contentContainerStyle = null,
  ...props
}: ScrollableProps) {
  const extraProps: ScrollViewProps = { testID: testScrollableID };
  extraProps.contentContainerStyle = contentContainerStyle || {
    paddingBottom: 50,
  };

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledScrollView {...extraProps} {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledScrollView {...extraProps} {...props} />
    </ThemeProvider>

  );
}

Scrollable.defaultProps = {};

export default Scrollable;
