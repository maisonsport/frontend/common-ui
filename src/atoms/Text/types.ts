import { TextProps as RNTextProps } from 'react-native';
import {
    FlexboxProps,
    LayoutProps,
    BorderProps,
    SpaceProps,
    BackgroundColorProps,
    ZIndexProps,
    styleFn,
    ColorProps,
    TypographyProps,
    FontFamilyProps,
    TextColorProps,
    TextAlignProps,
} from 'styled-system';

import { NoWrapTheme } from '../../types/global';

import { text as textVariants } from '../../theme';

type TextVariants = keyof typeof textVariants

type AdditionalProps = {
    a11y?: boolean
    a11yLabel?: string
    a11yRole?: string
    touchable?: boolean
    variant?: TextVariants
}

export type TextProps = RNTextProps
    & FlexboxProps
    & LayoutProps
    & BorderProps
    & ColorProps
    & SpaceProps
    & TextColorProps
    & TextAlignProps
    & BackgroundColorProps
    & TypographyProps
    & FontFamilyProps
    & NoWrapTheme
    & AdditionalProps
    & ZIndexProps
    & Partial<styleFn>