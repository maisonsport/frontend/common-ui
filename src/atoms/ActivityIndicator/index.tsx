// eslint-disable-next-line no-unused-vars
import React from 'react';
import { ActivityIndicator as RNActivityIndicator, ActivityIndicatorProps } from 'react-native';
import styled, { ThemeProvider } from 'styled-components/native';
import { variant } from 'styled-system';

import Theme, { activityIndicator as activityIndicatorV} from '../../theme/index';
import { NoWrapTheme } from '../../types/global';

const activityIndicatorVariants = variant({ scale: 'activityIndicator' });
const StyledActivityIndicator = styled.ActivityIndicator.attrs((props: ActivityIndicatorProps) => {
  const { color, size } = activityIndicatorVariants(props);
  return {
    color,
    size,
  };
})``;

interface Variants {
  variant?: keyof typeof activityIndicatorV
}
export interface Props extends ActivityIndicatorProps, NoWrapTheme, Variants {}

function ActivityIndicator({ noWrapTheme = false, ...props }: Props): React.ReactElement<RNActivityIndicator> {
  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledActivityIndicator {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledActivityIndicator {...props} />
    </ThemeProvider>
  );
}

export default ActivityIndicator;
