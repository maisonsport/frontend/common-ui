import * as React from "react";
import {BoxProps} from "../Box/types";
import Box from "../Box";
import Theme from "../../theme";

type CardProps = Omit<BoxProps, 'style'> & { shadow?: boolean };
const Card = ({ shadow, ...props }: CardProps) => {
	return (
		<Box
			width={'100%'}
			borderRadius={5}
			backgroundColor="white"
			padding={3}
			marginBottom={3}
			style={shadow ? Theme.globals.boxShadow : {}}
			{...props}
		/>
	);
};

export default Card;
