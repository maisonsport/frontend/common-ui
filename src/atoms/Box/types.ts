import { ViewProps, TouchableOpacityProps, ViewStyle } from 'react-native';
import {
    FlexboxProps,
    LayoutProps,
    BorderProps,
    SpaceProps,
    BackgroundColorProps,
    ZIndexProps,
    PositionProps,
    OpacityProps,
} from 'styled-system';

import { NoWrapTheme } from '../../types/global';

type AdditionalProps = {
    onPress?: () => void
    a11y?: boolean
    a11yLabel?: string
    a11yRole?: string
    disabled?: TouchableOpacityProps['disabled']
    style?: ViewStyle
}

export type BoxProps = ViewProps
    & FlexboxProps
    & LayoutProps
    & BorderProps
    & SpaceProps
    & BackgroundColorProps
    & NoWrapTheme
    & AdditionalProps
    & PositionProps
    & OpacityProps
    & ZIndexProps