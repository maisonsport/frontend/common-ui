/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  space,
  layout,
  borders,
  flexbox,
  opacity,
} from 'styled-system';
import A11y from 'accessible-system';

import Theme from '../../theme/index';

import { BoxProps } from './types';
import { ViewProps } from 'react-native';

export const testBoxID = 'box';

export const StyledView = styled.View.attrs(A11y)`
  ${space}
  ${borders}
  ${color}
  ${layout}
  ${flexbox}
  ${opacity}
`;

function Box({ noWrapTheme = false, ...props }: BoxProps) {
  const extraProps: ViewProps = { testID: testBoxID, pointerEvents: 'box-none' };

  const { onPress } = props;
  if (onPress) {
    delete extraProps.pointerEvents;
  }

  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledView {...extraProps} {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledView {...extraProps} {...props} activeOpacity={1} />
    </ThemeProvider>
  );
}

Box.defaultProps = {};

export default Box;
