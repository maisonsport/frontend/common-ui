// eslint-disable-next-line no-unused-vars
import React from 'react';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme/index';
import { StyledView } from '../Box';

const StyledHorizontalRule = StyledView;

function HorizontalRule({
  noWrapTheme = false,
  borderBottomColor = 'grey',
  borderBottomWidth = 1,
  width = 1,
  marginTop = 3,
  marginBottom = 3,
  ...props
}) {
  if (noWrapTheme) {
    // eslint-disable-next-line react/jsx-props-no-spreading
    return <StyledHorizontalRule {...props} />;
  }

  return (
    <ThemeProvider theme={Theme}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <StyledHorizontalRule {...props} />
    </ThemeProvider>
  );
}

export default HorizontalRule;
