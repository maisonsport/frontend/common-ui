import { TouchableOpacityProps } from 'react-native';
import {
    FlexboxProps,
    LayoutProps,
    BorderProps,
    SpaceProps,
    BackgroundColorProps,
    ZIndexProps,
    OpacityProps,
} from 'styled-system';

import { NoWrapTheme } from '../../types/global';

type AdditionalProps = {
    a11y?: boolean
    a11yLabel?: string
    a11yRole?: string
}

export type TouchableProps = TouchableOpacityProps
    & FlexboxProps
    & LayoutProps
    & BorderProps
    & SpaceProps
    & BackgroundColorProps
    & ZIndexProps
    & OpacityProps
    & NoWrapTheme
    & AdditionalProps