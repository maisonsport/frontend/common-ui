export const TEXT_INPUT_HEIGHT: number = 40;
export const SELECT_BOX_HEIGHT: number = TEXT_INPUT_HEIGHT;
export const BUTTON_HEIGHT: number = TEXT_INPUT_HEIGHT;