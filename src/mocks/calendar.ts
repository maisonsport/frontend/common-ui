import randomcolor from 'randomcolor';
import Fakerator from 'fakerator';

import { AbilityLevel, BookingStatus, BookingStatusState, BookingSummary, CancellationPolicy, Interval, ParticipantAge, PaymentStatus, PaymentStatusState, SlotType, Sport } from "../types/booking";
import { Currency } from '../types/currency';
import { Day, DayPricing, Rule, Slot } from '../types/calendar';

import { VISIBLE_HOURS } from '../constants/calendar';

import { getRandomSlotRecurrance, _getRandom, _getRandomBool, _getRandomElementOfEnum, _getRandomId, _getRandomNumber, _pickFromArray } from './common';
import { _getMockCustomer, _getMockInstructor } from './user';

const fakerator = Fakerator();

function _getRandomExternalProvider(populate: boolean): Pick<Slot, 'extService'|'title'> {
    return {
        extService: populate ? _getRandom('GMail', 'Apple') : '',
        title: populate ? fakerator.lorem.word() : '',
    };
}

function _getMockBookingSummary(): BookingSummary {
    return {
        resort: {
            name: fakerator.address.city(),
            slug: fakerator.address.city(),
            id: _getRandomId(true) as number,
            schoolRates: {
                day: 17000,
                hour: 1700,
            },
        },
        sport: _getRandomElementOfEnum<Sport>(Sport),
        abilityLevel: _getRandomElementOfEnum<AbilityLevel>(AbilityLevel),
        ageOfParticipants: _getRandomElementOfEnum<ParticipantAge>(ParticipantAge),
        cancellationPolicy: _getRandomElementOfEnum<CancellationPolicy>(CancellationPolicy),
        currency: _getRandomElementOfEnum<Currency>(Currency),
        instructor: _getMockInstructor(),
        customer: _getMockCustomer(),
        slots: [],
        numberOfParticipants: _getRandomNumber(),
        paymentSummary: {
            amountDue: _getRandomNumber(),
            bookingAmountDue: _getRandomNumber(),
            amountDueBy: new Date(),
            amountPaid: _getRandomNumber(),
            discount: {
                amount: _getRandomNumber(),
                code: fakerator.names.name(),
                description: fakerator.lorem.word(),
                name: fakerator.names.name(),
                percentage: _getRandomNumber(),
                removable: _getRandomBool(),
            },
            msFees: _getRandomNumber(),
            nextChargeAt: new Date(),
            paymentFees: _getRandomNumber(),
            subTotal: _getRandomNumber(),
            total: _getRandomNumber(),
        },
        actions: [],
        createdAt: new Date().toUTCString(),
        id: _getRandomId() as string,
        isProtected: _getRandomBool(),
        language: fakerator.address.countryCode(),
        paymentStatus: _getMockPaymentStatus(),
        referenceNumber: _getRandomId() as string,
        review: null,
        status: _getMockBookingStatus(),
        thread: null,
        transactionHistory: [],
        cancellationFee: 0,
        cancellationRefund: 0,
    };
}
interface GetMockTimeSlotArgs {
    fromDay?: number;
    fromHr?: number;
    fromMins?: number;
    toDay?: number;
    toHr?: number;
    toMins?: number;
    slotType?: SlotType;
}
export const _getMockTimeSlot = ({ fromDay = 22, fromHr = 9, fromMins = 0, toDay = 22, toHr = 11, toMins = 0, slotType }: GetMockTimeSlotArgs): Slot => {
    const isFullDay = _getRandomBool();
    const type = slotType || _getRandomElementOfEnum<SlotType>(SlotType);

    let start = new Date(2021, 11, fromDay, fromHr, fromMins, 0);
    let end = new Date(2021, 11, toDay, toHr, toMins, 0);
    if (isFullDay) {
        start = new Date(2021, 11, fromDay, VISIBLE_HOURS[0], 0);
        end = new Date(2021, 11, toDay, VISIBLE_HOURS[VISIBLE_HOURS.length - 1], 0);
    }

    const res: Slot = {
        type,
        __typename: 'Slot',
        id: randomcolor(),
        costPerInterval: _getRandomBool() ? 200 : 1000,
        interval: _getRandomBool() ? Interval.DAY : Interval.HOUR,
        intervals: 1,

        start,
        end,
        isFullDay,
        color: randomcolor(),
        title: fakerator.lorem.word(),

        ...getRandomSlotRecurrance([SlotType.BOOKING, SlotType.ENQUIRY, SlotType.REQUESTED_BOOKING].includes(type)),

        ..._getRandomExternalProvider(type === SlotType.EXTERNAL),
    };

    if (res.type !== SlotType.EXTERNAL) {
        res.customer = _getMockCustomer();
        res.bookingSummary = _getMockBookingSummary();
    }

    return res;
};

export const _getMockSchedule = (): Slot[] => {
    const times = [
        [8, 0, 9, 0],
        [8, 30, 9, 30],

        [9, 0, 10, 0],
        [9, 30, 10, 30],

        [10, 0, 11, 0],
        [10, 30, 11, 30],

        [11, 0, 12, 0],
        [11, 30, 12, 30],
        [10, 30, 11, 30],

        [12, 0, 14, 0],
        [12, 30, 14, 30],

        [14, 0, 16, 0],
        [14, 30, 16, 30],

        [8, 0, 12, 0],
        [8, 0, 17, 0],
        [12, 0, 18, 0],
    ];
    let slotCount = Math.floor(Math.random() * (times.length - 0 + 1)) + 0;
    if (slotCount > 5) slotCount = 5;

    return _pickFromArray(times, slotCount).map((time) => {
        const [fromHr, fromMins, toHr, toMins] = time;
        return _getMockTimeSlot({ fromHr, fromMins, toHr, toMins });
    });
};

export const _getMockRule = (): Rule => ({
    id: _getRandomId() as number,
    color: randomcolor(),
    name: fakerator.lorem.word(),
    times: _getRandom(['am', 'pm', 'fd']),
    minLessonsPerWeekday: [1, 2, 3, 4, 5, 6, 7],
});

export const _getMockDayPricing = (): DayPricing => ({
    perHourMorning: 9000,
    perHourAfternoon: 7000,
    perDay: 40000,
});

export const _getMockBookingStatus = (): BookingStatus => ({
    enteredAt: new Date().toUTCString(),
    state: _getRandomElementOfEnum<BookingStatusState>(BookingStatusState),
});

export const _getMockPaymentStatus = (): PaymentStatus => ({
    enteredAt: new Date().toUTCString(),
    state: _getRandomElementOfEnum<PaymentStatusState>(PaymentStatusState),
});

export const _getMockDay = (): Partial<Day> => {
    return {
        isInSeason: _getRandomBool(),
        rule: _getMockRule(),
        pricing: _getMockDayPricing(),
        slots: _getMockSchedule(),
    };
}
