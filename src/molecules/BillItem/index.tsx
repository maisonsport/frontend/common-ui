import * as React from "react";
import CardHeading from "../CardHeading";
import Box from "../../atoms/Box";
import Text from "../../atoms/Text";

type BillItemProps = {
	heading?: string;
	renderHeadingTooltip?: () => React.ReactElement;

	text: string | React.ReactElement;
	textFontWeight?: number;
	renderTooltip?: () => React.ReactElement;

	total?: string;

	/** Any money saved on item */
	saving?: string;

	breakdown?: string[];
};
const BillItem = (props: BillItemProps) => {
	return (
		<Box marginBottom={3} width={'100%'}>
			{!!props.heading && (
				<CardHeading
					text={props.heading}
					renderTooltip={props.renderHeadingTooltip}
					marginBottom={2}
					fontSize={2}
				/>
			)}
			<Box
				flexDirection={'row'}
				justifyContent={'space-between'}
				alignItems={'center'}
			>
				<Box
					flex={1}
					flexDirection={'row'}
					justifyContent={'space-between'}
				>
					<Box
						flexGrow={1}
						flexDirection={'row'}
						alignItems={'center'}
						alignSelf={'flex-start'}
					>
						{typeof props.text === 'string' ? (
							<Text
								fontSize={2}
								fontWeight={props.textFontWeight || 2}
								padding={0}
								marginRight={2}
							>
								{props.text}
							</Text>
						) : (
							<Box width={1} flexGrow={1}>
								{props.text}
							</Box>
						)}
						{!!props.renderTooltip && props.renderTooltip()}
					</Box>

					<Box>
						{!!props.total && (
							<Text
								fontSize={2}
								fontWeight={!!props.saving ? 6 : 3}
								color={!!props.saving ? 'green' : 'textGrey'}
								padding={0}
								textAlign={'right'}
							>
								{props.total}
							</Text>
						)}
						{!!props.saving && (
							<Text fontSize={0} color={'red'} padding={0} textAlign={'right'}>
								- {props.saving}
							</Text>
						)}
					</Box>
				</Box>
			</Box>
			{!!props.breakdown && (
				<Box marginTop={1}>
					{props.breakdown.map((item) => {
						return (
							<Text
								key={item}
								fontSize={0}
								padding={0}
								marginLeft={2}
								marginBottom={1}
							>
								{item}
							</Text>
						);
					})}
				</Box>
			)}
		</Box>
	);
};

export default BillItem;
