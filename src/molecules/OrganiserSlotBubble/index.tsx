import React from 'react';
import { Image } from 'react-native';
import { format, getHours, getMinutes, intervalToDuration, isSameDay } from 'date-fns';

import Box from '../../atoms/Box';
import Theme from '../../theme/index';
import Text from '../../atoms/Text';
import Avatar from '../Avatar';

import { BubbleContainer, BubbleInner } from './Containers';

import { TIME_BLOCK_HEIGHT, VISIBLE_HOURS } from '../../constants/calendar';

import { BaseBubbleProps, Slot } from '../../types/calendar';

import { getDifferenceInHours, percentageOfHour, getRemainingVisibleHours, formatStartEnd } from '../../util/calendar';
import { TranslatorFunc } from '../../types/global';
import { getCorrectTextColorForBackground } from '../../util/color';

function dateIsToday(selectedDay: Date, comparator: Date): boolean {
    return isSameDay(selectedDay, comparator);
}

export interface BubbleStyleProps {
    width?: number | string;
    left?: number | string;
    fullyRounded?: boolean;
    borderColor?: string;
}

interface Props extends BubbleStyleProps, BaseBubbleProps {}

const SlotBubble = ({
    onPress,
    selectedDay,
    start,
    end,
    isFullDay,

    width = 1,
    left = 0,
    fullyRounded = false,
    color,
    isFirstBubble,

    borderColor = void 0,

    children,
}: Props) => {
    const { hours, minutes } = intervalToDuration({
        start: start,
        end: end,
    });

    const isAlpha = dateIsToday(selectedDay, start);
    const isOmega = dateIsToday(selectedDay, end);

    const topOffsetMultiplier = +`${getDifferenceInHours({ hour: getHours(start) })}.${percentageOfHour(getMinutes(start))}`;

    let _isFullDay: boolean = !!isFullDay;
    if (!_isFullDay && !isAlpha && !isOmega) _isFullDay = true;

    let bubbleHeight = (TIME_BLOCK_HEIGHT * +`${hours}.${percentageOfHour(minutes).toFixed(0)}`);
    if (isAlpha && !isOmega) bubbleHeight = TIME_BLOCK_HEIGHT * getRemainingVisibleHours(getHours(start));
    if (_isFullDay) bubbleHeight = (TIME_BLOCK_HEIGHT * VISIBLE_HOURS.length);

    let top = TIME_BLOCK_HEIGHT * topOffsetMultiplier + 12.45;
    if (
        _isFullDay
        || (!isAlpha && isOmega)
    ) top = 12.45;

    const _s: any = { top };
    if (left) _s.left = left;

    const innerContainerProps: any = {};
    if (borderColor) {
        innerContainerProps.borderWidth = 1/2;
        innerContainerProps.borderColor = borderColor;
    }

    return (
        <BubbleContainer
            height={bubbleHeight}
            width={width}
            style={_s}
            onPress={onPress}
            paddingRight={fullyRounded ? Theme.space[1] / 4 : 0}
            paddingLeft={isFirstBubble ? Theme.space[1] / 4 : 0}
        >
            <BubbleInner
                style={{
                    shadowColor: Theme.colors.black,
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                }}
                fullyRounded={fullyRounded}
                backgroundColor={color || 'primary'}
                {...innerContainerProps}
            >
                {children}
            </BubbleInner>
        </BubbleContainer>
    )
}
export default SlotBubble;

interface BookingSlotProps extends Omit<BaseBubbleProps, 'isFullDay' | 'onPress' | 'children'>, Omit<Slot, 'start' | 'end'>, BubbleStyleProps {
    onPress: (slot: Slot) => void;
    small: boolean;

    t: TranslatorFunc;
}

SlotBubble.External = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const fromFormatted = format(slot.start, 'H:mm');
    const toFormatted = format(slot.end, 'H:mm');

    const textProps = {
        color: getCorrectTextColorForBackground(slot.color || Theme.colors.grey, { light: Theme.colors.white, dark: Theme.colors.textGrey }),
        fontWeight: 5,
        fontSize: small ? 0 : 1,
        padding: 0,
    };

    return (
        <SlotBubble
            color={slot.color || Theme.colors.grey}
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
        >
            <Box marginTop={2}>
                <Text marginBottom={small ? 2 : 0} {...textProps}>{fromFormatted} - {toFormatted}</Text>
                {slot.isFullDay && (
                    <Text fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                )}
                <Text {...textProps} numberOfLines={2} ellipsizeMode="tail">{slot.title}</Text>
            </Box>
        </SlotBubble>
    );
}

SlotBubble.Booking = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const textColor = getCorrectTextColorForBackground(slot.color || Theme.colors.primary, { light: Theme.colors.white, dark: Theme.colors.textGrey });

    return (
        <SlotBubble
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
        >
            <Box marginTop={2} flexDirection={small ? 'column' : "row"} alignItems={small ? 'center' : 'flex-start'} justifyContent="space-between">
                <Box justifyContent="space-between">
                    <Text color={textColor} fontWeight={5} fontSize={small ? 0 : 1} marginBottom={small ? 2 : 0} padding={0}>{formatStartEnd(slot.start, slot.end)}</Text>
                    {slot.isFullDay && (
                        <Text color={textColor} fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                    )}
                    {(!small && !!slot.customer) &&  <Text color={textColor} fontWeight={5} padding={0}>{slot.customer.displayName}</Text>}
                </Box>

                {!!(slot?.customer && slot.customer.displayName) && (
                    <Box>
                        <Avatar
                            size={small ? 30 : 32.5}
                            fontSize={Theme.fontSizes[1]}
                            name={slot.customer.displayName}
                            url={slot.customer.avatar?.url}
                        />
                    </Box>
                )}
            </Box>
            {(!small && slot?.customer && slot.customer?.phone && !slot.booking?.isProtected) && (
                <Box flexDirection="row" alignItems="center" marginTop={2}>
                    <Image source={require('../../assets/images/cal_phone.png')} style={{ width: 20, height: 20, marginRight: Theme.space[2] }} />
                    {/* @ts-ignore */}
                    <Text accessibilityRole='link' href={`tel:${slot.customer.phone}`} color={textColor} fontWeight={5} padding={0} numberOfLines={2} ellipsizeMode="tail">{slot?.customer?.phone||''}</Text>
                </Box>
            )}
        </SlotBubble>
    );
}

SlotBubble.UserCreated = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    const textColor = getCorrectTextColorForBackground(slot.color || Theme.colors.grey, { light: Theme.colors.white, dark: Theme.colors.textGrey });

    return (
        <SlotBubble
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
        >
            <Box marginTop={2} flexDirection={small ? 'column' : "row"} alignItems={small ? 'center' : 'flex-start'} justifyContent="space-between">
                <Box justifyContent="space-between">
                    <Text color={textColor} fontWeight={5} fontSize={small ? 0 : 1} marginBottom={small ? 2 : 0} padding={0}>{formatStartEnd(slot.start, slot.end)}</Text>
                    <Text color={textColor} fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{slot.title}</Text>
                </Box>
            </Box>
        </SlotBubble>
    );
}

SlotBubble.BookingRequest = ({ onPress, small, fullyRounded, left, width, t, ...slot }: BookingSlotProps): React.ReactElement => {
    return (
        <SlotBubble
            onPress={() => onPress(slot)}
            {...{ ...slot, fullyRounded, left, width }}
            color="white"
            borderColor="primary"
        >
            <Box marginTop={2} flexDirection={small ? 'column' : "row"} alignItems={small ? 'center' : 'flex-start'} justifyContent="space-between">
                <Box justifyContent="space-between">
                    <Text color="primary" fontWeight={5} fontSize={small ? 0 : 1} marginBottom={small ? 2 : 0} padding={0}>{formatStartEnd(slot.start, slot.end)}</Text>
                    {slot.isFullDay && (
                        <Text color="primary" fontWeight={5} fontSize={0} padding={0} marginBottom={1}>{t('text.general.full_day')}</Text>
                    )}
                    {(!small && !!slot.customer) && <Text color="primary" fontWeight={5} padding={0}>{slot.customer.displayName}</Text>}
                </Box>

                <Box>
                    <Avatar size={small ? 30 : 32.5} fontSize={Theme.fontSizes[1]} name={slot.customer?.displayName || ''} />
                </Box>
            </Box>
            {(!small && slot?.customer && slot.customer?.phone && !slot.booking?.isProtected) && (
                <Box flexDirection="row" alignItems="center" marginTop={2}>
                    <Image source={require('../../assets/images/cal_phone.png')} style={{ width: 20, height: 20, marginRight: Theme.space[2] }} />
                    <Text color="primary" fontWeight={5} padding={0} numberOfLines={2} ellipsizeMode="tail">{slot.customer.phone}</Text>
                </Box>
            )}
        </SlotBubble>
    );
}
