import * as React from "react";
import Text from "../../atoms/Text";
import Touchable from "../../atoms/Touchable";
import Theme from "../../theme";

export type NumberControlProps = {
	onPress: () => any;
	variant: 'minus' | 'add';
	disabled?: boolean;
};
const NumberControl = (props: NumberControlProps) => {
	let backgroundColor: string = Theme.colors.white;
	if (props.variant === 'add') backgroundColor = Theme.colors.primary;
	if (props.disabled) backgroundColor = Theme.colors.grey;

	let tintColor: string = Theme.colors.primary;
	if (props.variant === 'add') tintColor = Theme.colors.primary;
	if (props.disabled) tintColor = Theme.colors.grey;

	let textColor: string = Theme.colors.white;
	if (props.variant === 'minus') textColor = Theme.colors.primary;
	if (props.disabled) textColor = Theme.colors.white;

	let symbol: '-' | '+' = '-';
	if (props.variant === 'add') symbol = '+';

	const buttonSize: number = 25;
	return (
		<Touchable
			disabled={props.disabled}
			onPress={props.onPress}
			backgroundColor={backgroundColor}
			width={buttonSize}
			height={buttonSize}
			borderRadius={buttonSize / 2}
			alignItems="center"
			borderWidth={0.5}
			borderColor={tintColor}
			justifyContent="center"
			style={[!props.disabled && Theme.globals.boxShadow]}
		>
			<Text
				color={textColor}
				fontSize={1}
				fontWeight={6}
				margin={0}
				padding={0}
			>
				{symbol}
			</Text>
		</Touchable>
	);
};

export default NumberControl
