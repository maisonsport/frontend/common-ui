import React from 'react';
import { Animated, Image } from 'react-native';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme/index';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Scrollable from '../../atoms/Scrollable';
import Text from '../../atoms/Text';

import { SELECT_BOX_HEIGHT } from '../../constants/form';
import { BoxProps } from '../../atoms/Box/types';
import { TextProps } from '../../atoms/Text/types';

export const selectBoxID = 'select-box-id';
export const selectBoxIconID = 'select-box-icon';
export const selectBoxSelectedValueText = 'select-box-selected-value-text';

export const selectBoxOptionsContainer = 'select-box-options-container';
export const selectBoxOption = 'select-box-option';
export const selectBoxOptionIconID = 'select-box-option-icon';
export const selectBoxOptionTextID = 'select-box-option-text';
export const selectBoxOptionCheckID = 'select-box-option-check';

type Icon = string|((props: any) => React.ReactElement);

type OptionsHasDuplicates = {value: string}[];
function optionsHasDuplicates(opts: OptionsHasDuplicates) {
  const optionValues = opts.map(({ value }) => value);
  return optionValues.length !== [...new Set(optionValues)].length;
}

interface SelectIcon { Icon?: Icon; testID?: string; }
function SelectIcon({ Icon, ...props }: SelectIcon) {
  if (Icon) {
    if (typeof Icon === 'string') {
      const src = { uri: Icon };
      // eslint-disable-next-line jsx-a11y/alt-text
      return <Image source={src} {...props} />;
    }
    return <Icon {...props} />;
  }

  return null;
}

interface OPTION {
  value: string;
  label: string;
  Icon?: Icon
}

const SELECT_HEIGHT = SELECT_BOX_HEIGHT;

export interface SelectBoxProps {
  placeholder: string;
  value: string;
  options: OPTION[];
  onSelect: (selectedValue: string) => void;
  Icon?: Icon;
  iconPosition?: 'right'|'left';
  disabled?: boolean
  optionContainerProps?: BoxProps
  inputContainerProps?: BoxProps
  inputContainerTextProps?: TextProps
  onDropdownOpen?: () => void
  onDropdownClose?: () => void
}
function SelectBox({
  placeholder,
  value,
  options,
  onSelect,
  iconPosition = 'right',
  Icon,
  disabled = false,
  optionContainerProps = {},
  inputContainerProps = {},
  inputContainerTextProps = {},

  onDropdownOpen = () => {},
  onDropdownClose = () => {},
}: SelectBoxProps) {
  if (optionsHasDuplicates(options)) {
    // eslint-disable-next-line no-console
    console.warn('The options you have provided have multiples of the same `value`, this will cause rendering issues. Make sure each option has a unique `value` key.');
  }

  const [open, setOpen] = React.useState(false);
  const [highlighted, setHighlighted] = React.useState('');
  const [chevronPos] = React.useState(new Animated.Value(0));

  React.useEffect(() => {
    if (open) onDropdownOpen();
    else onDropdownClose();

    Animated
      .timing(chevronPos, { toValue: open ? 1 : 0, duration: 300, useNativeDriver: true })
      .start();
  }, [open]);

  const displayValue = options.find(({ value: oV }) => oV === value)?.label || placeholder;

  const commonItemProps: BoxProps = {
    flexDirection: 'row',
    alignItems: 'center',

    paddingLeft: 2,
    paddingRight: 2,

    borderWidth: 0.5,
    borderColor: 'grey',
  };

  const chevronAngle = chevronPos.interpolate({
    inputRange: [0, 1],
    outputRange: ['90deg', '-90deg'],
  });

  return (
    <ThemeProvider theme={Theme}>
      <Box noWrapTheme style={{ position: 'relative' }}>
        <Touchable
          disabled={disabled}
          testID={selectBoxID}
          noWrapTheme
          justifyContent="space-between"
          backgroundColor="white"
          height={SELECT_HEIGHT}
          borderRadius={5}
          borderBottomLeftRadius={open ? 0 : 5}
          borderBottomRightRadius={open ? 0 : 5}
          zIndex={0}
          opacity={disabled ? 0.5 : 1}
          onPress={() => setOpen((o) => !o)}
          {...commonItemProps}
          {...inputContainerProps}
        >
          <Box noWrapTheme flexDirection="row" alignItems="center">
            {iconPosition === 'right' && <Text touchable={false} noWrapTheme padding={0} marginRight={1} color={value ? 'textGrey' : 'placeholderGrey'} fontSize={1} testID={selectBoxSelectedValueText} {...inputContainerTextProps}>{displayValue}</Text>}
            <SelectIcon {...{ Icon }} testID={selectBoxIconID} />
            {iconPosition === 'left' && <Text touchable={false} noWrapTheme padding={0} marginLeft={1} color={value ? 'textGrey' : 'placeholderGrey'} fontSize={1} testID={selectBoxSelectedValueText} {...inputContainerTextProps}>{displayValue}</Text>}
          </Box>
          <Animated.Image
            source={require('../../assets/images/chevron.png')}
            style={{
              width: 10,
              height: 10,
              transform: [{ rotate: chevronAngle }]
            }}
          />
        </Touchable>
        {open && (
          <Scrollable
            testID={selectBoxOptionsContainer}
            noWrapTheme
            borderBottomLeftRadius={5}
            borderBottomRightRadius={5}
            borderWidth={commonItemProps.borderWidth}
            borderTopWidth={0}
            borderColor={commonItemProps.borderColor}
            overflow="hidden"
            // @ts-ignore
            onMouseLeave={() => setHighlighted('')}
            maxHeight={225}
            style={{
              position: 'absolute',
              top: SELECT_HEIGHT,
              width: '100%',
            }}
            contentContainerStyle={{paddingBottom: 0}}
          >
            {options.map(({ value: optValue, label, Icon: optIcon }, i) => {
              const isSelected = optValue === value;
              const isHighlighted = optValue === highlighted;

              return (
                <Touchable
                  testID={`${selectBoxOption}_${optValue}`}
                  noWrapTheme
                  zIndex={0}
                  key={optValue}
                  backgroundColor={isHighlighted ? 'lightGrey' : isSelected ? 'darkGrey' : 'white'}
                  justifyContent="space-between"
                  onPress={() => {
                    onSelect(optValue);
                    setOpen(false);
                  }}
                  // @ts-ignore
                  onMouseEnter={() => setHighlighted(optValue)}
                  {...commonItemProps}
                  height={SELECT_HEIGHT}
                  borderBottomWidth={i === options.length - 1 ? 0 : commonItemProps.borderWidth}
                >
                  <Box width={1} noWrapTheme pointerEvents="none" flexDirection="row" alignItems="center" {...optionContainerProps}>
                    {iconPosition === 'right' && <Text color={isHighlighted ? 'textGrey' : isSelected ? 'white' : 'textGrey'} fontWeight={isSelected ? 'bold' : 'medium'} noWrapTheme touchable={false} padding={0} fontSize={1} testID={`${selectBoxOptionTextID}_${optValue}`}>{label}</Text>}
                    <SelectIcon {...{ Icon: optIcon }} testID={`${selectBoxOptionIconID}_${optValue}`} />
                    {iconPosition === 'left' && <Text color={isHighlighted ? 'textGrey' : isSelected ? 'white' : 'textGrey'} fontWeight={isSelected ? 'bold' : 'medium'} noWrapTheme touchable={false} padding={0} fontSize={1} testID={`${selectBoxOptionTextID}_${optValue}`}>{label}</Text>}
                  </Box>
                </Touchable>
              );
            })}
          </Scrollable>
        )}
      </Box>
    </ThemeProvider>
  );
}

export default SelectBox;
