/* eslint-disable no-console */
import React, { useState } from 'react';
import { Image } from 'react-native';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme/index';

import { getRandomColor } from '../../util/functions';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

export const avatarContainerTestID = 'avatar-container';
export const avatarContentTestID = 'avatar-content';
export const avatarImageTestID = 'avatar-image';

export interface Props {
  name: string;
  size?: number;
  fontSize?: number;
  url?: string;
}

function Avatar({
  name = '',
  size = 40,
  fontSize = Theme.text.h3.fontSize,
  url = '',
}: Props) {
  const avatarSource = { uri: url };
  const [showAsImage, setShowAsImage] = useState(!!avatarSource.uri);
  const names = name.split(' ');
  const initials = (names.length > 2 ? [
    names[0],
    names[1],
    names[names.length - 1],
  ] : [
    names[0],
    names[1],
  ]).filter(Boolean).map(([initial]) => initial.toUpperCase()).join('');
  return (
    <ThemeProvider theme={Theme}>
      <Box testID={avatarContainerTestID} noWrapTheme width={size} height={size} backgroundColor={getRandomColor(name)} borderRadius={size} alignItems="center" justifyContent="center">
        {showAsImage ? (
          <Image
            testID={avatarImageTestID}
            source={avatarSource}
            onError={() => {
              setShowAsImage(false);
            }}
            style={{
              width: size,
              height: size,
              borderRadius: size,
            }}
          />
        ) : (
          <Text testID={avatarContentTestID} noWrapTheme variant="h3" color="white" margin={0} padding={0} fontSize={fontSize}>{initials}</Text>
        )}
      </Box>
    </ThemeProvider>
  );
}

export default Avatar;
