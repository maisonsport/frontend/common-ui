import React from 'react';
import initials from 'initials';
import { intervalToDuration } from 'date-fns';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

import { Props as MonthViewCalendarProps } from '../../organisms/MonthViewCalendar';

import { BaseBubbleProps, Slot } from '../../types/calendar';

import { getCorrectTextColorForBackground } from '../../util/color';

import Theme from '../../theme/index';

export const bubbleContainerTestId = 'bubble_container_test_id';
export const bubbleInnerContainerTestId = 'bubble_inner_container_test_id';
export const bubbleTextTestId = 'bubble_text_test_id';

type Viewport = Pick<MonthViewCalendarProps, 'viewport'>;
export interface Props extends Viewport {
    color?: string;
    start: Slot['start'];
    end: Slot['end'];

    borderColor?: string;
    opacity?: number;
    children: any;
}

const BubbleContainer = (props: any) => <Box padding={1.5} {...props} />;
const BubbleInner = (props: any) => <Box alignSelf="center" width={1} flex={1} borderRadius={5} {...props} />

const SlotBubble = ({ start, end, color, children, borderColor, opacity = 1 }: Props) => {
    const { hours = 1 } = intervalToDuration({
        start,
        end,
    });

    const innerContainerProps: any = {};
    if (borderColor) {
        innerContainerProps.borderWidth = 1 / 2;
        innerContainerProps.borderColor = borderColor;
    }

    return (
        <BubbleContainer testID={bubbleContainerTestId} flex={1} height={`${(hours * 10)}%`}>
            <BubbleInner
                testID={bubbleInnerContainerTestId}
                alignItems="center"
                justifyContent="center"
                backgroundColor={color || 'primary'}
                padding={1}
                opacity={opacity}
                {...innerContainerProps}
            >
                {children}
            </BubbleInner>
        </BubbleContainer>
    );
}

export default SlotBubble;

export type BaseWithoutOnPressAndChildren = Omit<BaseBubbleProps, 'isFirstBubble' | 'onPress' | 'children' | 'selectedDay'> & Slot & Viewport;
SlotBubble.External = ({ title, start, end, color, viewport }: BaseWithoutOnPressAndChildren&Slot&Viewport): React.ReactElement => {
    const textColor = getCorrectTextColorForBackground(color || Theme.colors.grey);

    let _title: string | string[] | undefined = title;
    if (viewport === 'MOBILE' && title?.length && title.length > 6) {
        _title = initials(title);
    }
    if ((!viewport || viewport === 'DESKTOP') && title?.length && title.length > 10) {
        _title = `${title.slice(0, 6)}...`
    }
    return (
        <SlotBubble color={color} start={start} end={end}>
            <Text testID={bubbleTextTestId} color={textColor} fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{_title}</Text>
        </SlotBubble>
    );
};

SlotBubble.Booking = ({ customer, start, end, color }: BaseWithoutOnPressAndChildren): React.ReactElement => {
    const textColor = getCorrectTextColorForBackground(color || Theme.colors.primary);
    return (
        <SlotBubble start={start} end={end} color={color}>
            {(customer && customer?.displayName) && <Text testID={bubbleTextTestId} color={textColor} fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{initials(customer.displayName)}</Text>}
        </SlotBubble>
    );
};

SlotBubble.BookingRequest = ({ customer, start, end }: BaseWithoutOnPressAndChildren): React.ReactElement => {
    return (
        <SlotBubble start={start} end={end} color="white" borderColor="primary">
            {(customer && customer?.displayName) && <Text testID={bubbleTextTestId} color="primary" fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{initials(customer.displayName)}</Text>}
        </SlotBubble>
    );
};

SlotBubble.UserCreated = ({ title, start, end, color, viewport }: BaseWithoutOnPressAndChildren): React.ReactElement => {
    const textColor = getCorrectTextColorForBackground(color || Theme.colors.white);

    let _title: string|string[]|undefined = title;
    if (viewport === 'MOBILE' && title?.length && title.length > 6) {
        _title = initials(title);
    }
    if ((!viewport || viewport === 'DESKTOP') && title?.length && title.length > 10) {
        _title = `${title.slice(0, 6)}...`
    }
    return (
        <SlotBubble start={start} end={end} color={color} borderColor={color}>
            {title && <Text testID={bubbleTextTestId} color={textColor} fontSize={0} padding={0} numberOfLines={1} ellipsizeMode="tail">{_title}</Text>}
        </SlotBubble>
    );
};
