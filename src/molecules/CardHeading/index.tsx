
import {BoxProps} from "../../atoms/Box/types";
import Text from "../../atoms/Text";
import Box from "../../atoms/Box";
import * as React from "react";

type CardHeadingProps = {
	text: string;
	renderTooltip?: () => React.ReactElement;

	fontSize?: number;
} & Pick<BoxProps, 'marginBottom' | 'opacity'>;
const CardHeading = ({ text, renderTooltip, fontSize = 3, ...props }: CardHeadingProps) => {
	return (
		<Box
			flexDirection={'row'}
			alignItems={'center'}
			marginBottom={3}
			{...props}
		>
			<Text
				fontSize={fontSize}
				fontWeight={5}
				marginRight={1}
				padding={0}
				color={'textGrey'}
			>
				{text}
			</Text>
			{!!renderTooltip && renderTooltip()}
		</Box>
	);
};

export default CardHeading;
