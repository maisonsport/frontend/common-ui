import * as React from 'react';
import Theme from '../../theme';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';
import Box from '../../atoms/Box';

export type TabProps = {
  text: string;
  renderTooltip?: () => React.ReactElement;
  isSelected: boolean;
  isDirty?: boolean;

  onPress: () => any;
};
const Tab = (props: TabProps) => {
  const backgroundColor: string = props.isSelected
    ? Theme.colors.lightBlue
    : Theme.colors.white;
  const textColor: string = props.isSelected
    ? Theme.colors.primary
    : Theme.colors.textGrey;

  const borderWidth: number = 0.5;
  const borderConfig: any = props.isSelected
    ? {
        borderColor: Theme.colors.primary,
        borderWidth,
        borderBottomWidth: 0,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
      }
    : {
        borderWidth,
        borderColor: Theme.colors.grey,
        borderBottomColor: Theme.colors.primary,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
      };

  return (
    <Touchable
      {...borderConfig}
      onPress={props.onPress}
      flex={1}
      backgroundColor={backgroundColor}
      flexDirection={'row'}
      alignItems={'center'}
      justifyContent={'center'}
      paddingTop={1}
      paddingBottom={1}
      style={{ position: 'relative' }}
    >
      <Box flexDirection={'row'} alignItems={'center'}>
        <Text fontSize={2} fontWeight={3} color={textColor}>
          {props.text}
        </Text>
        {!!props.renderTooltip && props.renderTooltip()}
      </Box>

      {props.isDirty && (
        <Box
          width={11}
          height={11}
          borderRadius={5.5}
          backgroundColor={'primary'}
          style={{
            position: 'absolute',
            top: Theme.space[2],
            right: Theme.space[2],
          }}
        />
      )}
    </Touchable>
  );
};

export default Tab;
