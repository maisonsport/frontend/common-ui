import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme/index';

import Touchable from '../../atoms/Touchable';
import ActivityIndicator from '../../atoms/ActivityIndicator';
import { BoxProps } from '../../atoms/Box/types';

export const messageSendButtonTestID = 'message-button';
export const messageSendButtonIconID = 'message-button-icon';
export const messageSendBusyIconTestID = 'message-button-busy-icon';

export interface Props extends BoxProps {
  busy?: boolean
  disabled?: boolean
  width?: string|number
  height?: string|number
  onPress: () => any
}

function MessageSendButton({
  busy = false,
  disabled = false,
  width = 30,
  height = 30,
  onPress,
}: Props) {
  return (
    <ThemeProvider theme={Theme}>
      <Touchable
        testID={messageSendButtonTestID}
        noWrapTheme
        width={width}
        height={height}
        borderRadius={width || height}
        alignItems="center"
        justifyContent="center"
        backgroundColor={(disabled || busy) ? Theme.colors.darkGrey : Theme.colors.primary}
        marginRight={3}
        onPress={onPress}
        disabled={disabled || busy}
      >
        {busy ? (
          <ActivityIndicator testID={messageSendBusyIconTestID} noWrapTheme variant="secondary" />
        ) : (
          <Image testID={messageSendButtonIconID} source={require('../../assets/images/send.png')} style={{ width: 15, height: 15, tintColor: Theme.colors.white }} />
        )}
      </Touchable>
    </ThemeProvider>
  );
}

export default MessageSendButton;
