import React, {
  createRef, forwardRef, HTMLInputTypeAttribute, useEffect, useState,
} from 'react';
import { TextInput as RNTextInput, TextInputProps } from 'react-native';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color, layout, space, typography, fontFamily,
} from 'styled-system';
import composeRefs from '@seznam/compose-react-refs';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';
import Theme from '../../theme/index';
import { TEXT_INPUT_HEIGHT } from '../../constants/form';
import { BoxProps } from '../../atoms/Box/types';

export const inputParentID = 'input-parent';
export const errorMessageTestID = 'input-error-message';
export const placeholderErrorIconID = 'input-placeholder-error-icon';
export const inputContainerID = 'input-container';
export const inputIconID = 'input-icon';
export const inputID = 'text-input';
export const inputColors = { error: 'red', default: 'white' };
export const inputMargins = { error: 1, default: 0 };

const StyledTextInput = styled.TextInput.attrs(({ theme }) => ({
  placeholderTextColor: theme.colors.placeholderGrey,
}))`
  ${color}
  ${typography}
  ${layout}
  ${space}
  ${fontFamily}
`;
StyledTextInput.defaultProps = {
  color: Theme.colors.textGrey,
  backgroundColor: inputColors.default,
  fontFamily: 0,
  fontWeight: 3,
  fontSize: 2,
  height: TEXT_INPUT_HEIGHT,
  flex: 1,
  width: 1,
  placeholderStyle: {
    font: 0,
  },
};

interface ErrorIconComponentProps {
  width: number
  height: number
  stroke: string
}

function getErrorMessages(errorMessages: Props['errorsMessages']) {
  if (!errorMessages) return null;

  return errorMessages.filter(Boolean).map((message: string) => (
    <Box key={message} flexDirection="row" marginBottom={1} alignItems="center">
      <Box
        width="7.5px"
        height="7.5px"
        backgroundColor={inputColors.error}
        borderRadius="7.5px"
      />
      <Text
        testID={errorMessageTestID}
        noWrapTheme
        variant="p"
        color={inputColors.error}
        fontWeight={4}
        marginLeft={1}
        textAlign="left"
      >
        {message}
      </Text>
    </Box>
  ));
}
interface Props extends TextInputProps {
  type?: HTMLInputTypeAttribute|null
  icon?: React.ReactNode
  iconRight?: React.ReactNode
  error?: boolean
  errorsMessages?: [string | boolean]
  backgroundColor?: string|null
  ErrorIconComponent?: (props: ErrorIconComponentProps) => React.ReactElement

  inputContainerProps?: BoxProps

  [key: string]: any
}

const TextInput = forwardRef(({
  noWrapTheme,
  type = null,
  icon = null,
  iconRight = null,
  error = false,
  errorMessages = [],
  ErrorIconComponent = () => (
    <Text
      fontWeight="black"
      color={inputColors.error}
      testID={placeholderErrorIconID}
    >
      X
    </Text>
  ),
  inputContainerProps = {},
  backgroundColor = null,
  marginBottom,
  marginTop,
  marginLeft,
  marginRight,
  ...props
}: Props, externalRef) => {
  const inputRef = createRef<RNTextInput>();
  const [componentHasError, setComponentHasError] = useState(false);

  useEffect(() => {
    setComponentHasError(error || !!errorMessages.filter(Boolean).length);
  }, [error, errorMessages.filter(Boolean).length]);

  if (backgroundColor) inputColors.default = backgroundColor;

  function getRightSideIcon() {
    const Container = ({ onPress = () => {}, children, ...props }: any) => (
      <Touchable
        noWrapTheme
        accessible={false}
        backgroundColor={inputColors.default}
        width="8%"
        height={1}
        justifyContent="center"
        alignItems="center"
        onPress={onPress}
        activeOpacity={1}
        tabIndex={-1}
        style={{ cursor: 'text' }}
        {...props}
      >
        {children}
      </Touchable>
    );
    let rightSideIcon = <></>;
    if (componentHasError) {
      rightSideIcon = (
        <Container
          onPress={() => inputRef?.current?.focus()}
        >
          <ErrorIconComponent
            width={15}
            height={15}
            stroke={Theme.colors[inputColors.error]}
          />
        </Container>
      );
    } else if (iconRight) {
      rightSideIcon = (
        <Container pointerEvents="box-none">
          {iconRight}
        </Container>
      );
    }

    return rightSideIcon;
  }

  return (
    <ThemeProvider theme={Theme}>
      <Box
        testID={`${props.placeholder || ''}_${inputParentID}`}
        noWrapTheme
        marginTop={marginTop}
        marginRight={marginRight}
        marginBottom={marginBottom}
        marginLeft={marginLeft}
      >
        <Box
          testID={inputContainerID}
          noWrapTheme
          height={props.height || 'textInputHeight'}
          backgroundColor={inputColors.default}
          borderColor={componentHasError ? inputColors.error : Theme.colors.grey}
          borderWidth={0.5}
          borderRadius={4}
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
          marginBottom={componentHasError ? inputMargins.error : inputMargins.default}
          overflow="hidden"
          {...inputContainerProps}
        >
          {icon && (
            <Box
              testID={inputIconID}
              noWrapTheme
              accessible={false}
              width="8%"
              height={1}
              backgroundColor={inputColors.default}
              justifyContent="center"
              alignItems="center"
              zIndex={-1}
            >
              {icon}
            </Box>
          )}
          <StyledTextInput
            testID={inputID}
            paddingLeft={1}
            autoCapitalize={(props?.type && ['email'].includes(props.type)) ? 'none' : 'sentences'}
            // eslint-disable-next-line react/jsx-props-no-spreading
            {...props}
            ref={composeRefs(inputRef, externalRef)}
          />
          {getRightSideIcon()}
        </Box>
        {componentHasError && (
          <Box marginY={1}>
            {getErrorMessages(errorMessages)}
          </Box>
        )}
      </Box>
    </ThemeProvider>
  );
});

TextInput.displayName = 'TextInput';

// @ts-ignore
TextInput.getErrorMessages = getErrorMessages;

export default TextInput;
