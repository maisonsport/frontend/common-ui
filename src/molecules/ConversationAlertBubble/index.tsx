import React from 'react';
import { ThemeProvider } from 'styled-components/native';

import Theme from '../../theme/index';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

export const conversationAlertBubbleTestID = 'conversation-alert-bubble';
export const conversationAlertBubbleContentTestID = 'conversation-alert-content';

interface Props {
  text: string
}
function ConversationAlertBubble({ text }: Props) {
  return (
    <ThemeProvider theme={Theme}>
      <Box noWrapTheme padding={2} width={1}>
        <Box
          testID={conversationAlertBubbleTestID}
          noWrapTheme
          width={1}
          padding={3}
          backgroundColor="lightRed"
          borderRadius={10}
        >
          <Text
            testID={conversationAlertBubbleContentTestID}
            noWrapTheme
            fontSize={1}
            color="white"
            textAlign="center"
            fontWeight={4}
          >
            {text}
          </Text>
        </Box>
      </Box>
    </ThemeProvider>
  );
}

export default ConversationAlertBubble;
