import { TouchableOpacityProps, ViewProps } from 'react-native';
import {
    LayoutProps,
    BorderProps,
    SpaceProps,
    BackgroundColorProps,
    ZIndexProps,
    OpacityProps,
} from 'styled-system';

import { NoWrapTheme } from '../../types/global';

import { buttons as buttonVariants } from '../../theme';
import React from 'react';
import { TouchableProps } from '../../atoms/Touchable/types';

type ButtonVariants = keyof typeof buttonVariants

type AdditionalProps = {
    variant?: ButtonVariants
    text: string
    translate?: string|null
    format?: {}
    busy?: boolean
    iconLeft?: React.ReactNode
    iconRight?: React.ReactNode
    TextComponent?: any
    activeOpacity?: TouchableOpacityProps['activeOpacity']
    style?: ViewProps['style']
}

export type ButtonProps = TouchableProps
    & SpaceProps
    & BorderProps
    & BackgroundColorProps
    & LayoutProps
    & ZIndexProps
    & OpacityProps
    & NoWrapTheme
    & AdditionalProps