/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { ThemeProvider } from 'styled-components/native';
import {
  color,
  backgroundColor,
  space,
  layout,
  borders,
  borderRadius,
  variant,
  position,
  width,
} from 'styled-system';

import Theme from '../../theme/index';

import ActivityIndicator, {
  Props as ActivityIndicatorProps,
} from '../../atoms/ActivityIndicator';
import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';
import { TextProps } from '../../atoms/Text/types';

import { ButtonProps } from './types';
import { BUTTON_HEIGHT } from '../../constants/form';

const buttonVariant = variant({ scale: 'buttons' });
const StyledButton = styled(Touchable)`
  ${buttonVariant}
  ${space}
  ${width}
  ${borders}
  ${borderRadius}
  ${color}
  ${layout}
  ${position}
`;

export const buttonTestID = 'button-id';

function Button({
  TextComponent = Text,
  text,
  translate = null,
  format = {},
  busy = false,
  iconLeft = null,
  iconRight = null,
  noWrapTheme = false,
  ...props
}: ButtonProps) {
  const { variant = 'primary', activeOpacity = 0.9 } = props;
  const defaultContainerStyle = { flex: 1 };
  const containerStyle = [defaultContainerStyle, props.style || {}];

  const activityIndicatorProps: ActivityIndicatorProps = {
    // eslint-disable-next-line react/destructuring-assignment
    // @ts-ignore
    variant: `button_${variant}`,
    noWrapTheme: true,
  };
  const buttonProps: Partial<ButtonProps> = {
    ...props,
    variant,
    activeOpacity,
    style: containerStyle,
  };

  // eslint-disable-next-line react/destructuring-assignment
  if (variant === 'disabled') {
    buttonProps.disabled = true;
    activityIndicatorProps.variant = 'button_disabled';
  }
  if (busy) {
    buttonProps.disabled = true;
    buttonProps.busy = true;
  }

  const textProps: TextProps = {
    noWrapTheme: true,
    // @ts-ignore
    variant: `button_${variant}`,
  };

  let cursor = 'pointer';
  if (busy) cursor = 'wait';
  // eslint-disable-next-line react/destructuring-assignment
  if (variant === 'disabled') cursor = 'not-allowed';
  if (buttonProps.style) {
    // @ts-ignore
    buttonProps.style.cursor = cursor;
  }

  return (
    <ThemeProvider theme={Theme}>
      <StyledButton
        testID={buttonTestID}
        {...buttonProps}
        height="buttonHeight"
        minHeight="buttonHeight"
        maxHeight="buttonHeight"
      >
        <Box
          noWrapTheme
          flexDirection="row"
          justifyContent="space-evenly"
          height={BUTTON_HEIGHT}
        >
          <Box noWrapTheme width={7} justifyContent="center">
            {iconLeft}
          </Box>
          <Box
            noWrapTheme
            flexGrow={1}
            alignItems="center"
            justifyContent="center"
          >
            <TextComponent translate={translate} format={format} {...textProps}>
              {text}
            </TextComponent>
          </Box>
          <Box noWrapTheme width={7} justifyContent="center">
            {busy ? (
              <ActivityIndicator {...activityIndicatorProps} />
            ) : (
              iconRight
            )}
          </Box>
        </Box>
      </StyledButton>
    </ThemeProvider>
  );
}

export default Button;
