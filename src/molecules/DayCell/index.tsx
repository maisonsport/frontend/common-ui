import React from 'react';
import { isSameDay, isToday } from 'date-fns';
import md5 from 'md5';

import Box from '../../atoms/Box';
import Touchable from '../../atoms/Touchable';
import Text from '../../atoms/Text';

import { Props as MonthViewCalendarProps } from '../../organisms/MonthViewCalendar';

import Theme from '../../theme/index';

import useCurrencyFormatter from '../../hooks/useCurrencyFormatter';

import { I18nLocale } from '../../types/global';
import { Currency, CalendarTypes, BookingTypes } from '../../types';

import { CALENDAR_GENERAL_BORDER_WIDTH } from '../../constants/calendar';

import SlotBubble from '../DayCellSlotBubble';
import {BookingStatusState} from "../../types/booking";

export interface Props extends CalendarTypes.Day, Pick<MonthViewCalendarProps, 'viewport'> {
    onViewDay: () => void;
    view: 'bookings' | 'pricing';
    selectedDay: Date;
    dayIndex: number;
    locale: I18nLocale;
    currency: Currency;
}

const SELECTED_COLOR = Theme.colors.darkPrimary;

const DayCell = ({
    rule,
    slots,
    view,
    isInSeason,
    type,
    date,
    iso,
    pricing,
    currency,
    onViewDay,
    selectedDay,
    locale,
    viewport,
}: Props) => {
    const { format } = useCurrencyFormatter({ locale });

    const isNotDayOfCurrentMonth = (type === 'next' || type === 'previous');
    const dayIsSelected = isSameDay(iso, selectedDay);

    const BOX_PROPS: any = {
        style: {
            outlineColor: Theme.colors.lightGrey,
            outlineStyle: "solid",
            outlineWidth: CALENDAR_GENERAL_BORDER_WIDTH/2,
        },
    };
    const TEXT_PROPS: any = {};
    const PRICE_TEXT_PROPS: any = {};

    if (isToday(iso)) {
        BOX_PROPS.backgroundColor = Theme.colors.lightPrimary;
        TEXT_PROPS.color = 'white';
        TEXT_PROPS.fontWeight = 5;
        PRICE_TEXT_PROPS.color = 'white'
    }

    if (!isInSeason) {
        BOX_PROPS.backgroundColor = Theme.colors.grey;
        TEXT_PROPS.color = 'textGrey';
    }

    if (isNotDayOfCurrentMonth) {
        BOX_PROPS.backgroundColor = Theme.colors.lightGrey;
        TEXT_PROPS.color = 'grey';
    }

    if (dayIsSelected) {
        BOX_PROPS.backgroundColor = SELECTED_COLOR;
        TEXT_PROPS.color = 'white';
        PRICE_TEXT_PROPS.color = 'white'
    }

    let dotColor: string = 'transparent';
    if (rule) {
        dotColor = rule.color;
        if (isNotDayOfCurrentMonth) {
            dotColor = Theme.colors.textGrey;
        }
    }

    return (
        <Touchable flex={1} minHeight="100%" onPress={onViewDay}>
            <Box
                flex={1}
                justifyContent="flex-start"
                {...BOX_PROPS}
            >
                <Box height="25%" justifyContent="center" alignItems="center">
                    <Text
                        marginTop={1}
                        textAlign="center"
                        fontSize={1}
                        fontWeight={6}
                        padding={0}
                        color="textGrey"
                        {...TEXT_PROPS}
                    >
                        {date}
                    </Text>

                    <Box
                        alignSelf="center"
                        width="5px"
                        height="5px"
                        borderRadius="2.5px"
                        backgroundColor={dotColor || 'transparent'}
                    />
                </Box>
                <Box height="75%" padding={1} paddingTop={0}>
                    {view === 'bookings' && (
                        slots
                            .sort((a, b) => a.start > b.start ? 1 : -1)
                            .map((slot) => {
                                const bubbleOpacity = isNotDayOfCurrentMonth ? 0.5 : 1;

                                if (slot.type === BookingTypes.SlotType.ENQUIRY) {
                                    return null;
                                }

                                if (slot.type === BookingTypes.SlotType.EXTERNAL) {
                                    return (
                                        <SlotBubble.External
                                            key={md5(JSON.stringify(slot))}
                                            {...slot}
                                            color={!isInSeason ? Theme.colors.darkGrey : slot.color || void 0}
                                            opacity={bubbleOpacity}
                                            viewport={viewport}
                                        />
                                    );
                                }

                                if (slot.type === BookingTypes.SlotType.REQUESTED_BOOKING) {
                                    return (
                                        <SlotBubble.BookingRequest
                                            key={md5(JSON.stringify(slot))}
                                            {...slot}
                                            color={!isInSeason ? Theme.colors.darkGrey : slot.color || void 0}
                                            opacity={bubbleOpacity}
                                            viewport={viewport}
                                        />
                                    );
                                }

                                if (slot.type === BookingTypes.SlotType.INSTRUCTOR) {
                                    return (
                                        <SlotBubble.UserCreated
                                            key={md5(JSON.stringify(slot))}
                                            {...slot}
                                            color={!isInSeason ? Theme.colors.darkGrey : slot.color || void 0}
                                            opacity={bubbleOpacity}
                                            viewport={viewport}
                                        />
                                    );
                                }

                                if (slot.booking?.status.state === BookingStatusState.CANCELLEDBYINSTRUCTOR) {
                                    slot.color = Theme.colors.lightRed;
                                }

                                return (
                                    <SlotBubble.Booking
                                        key={md5(JSON.stringify(slot))}
                                        {...slot}
                                        color={!isInSeason ? Theme.colors.darkGrey : slot.color || void 0}
                                        opacity={bubbleOpacity}
                                        viewport={viewport}
                                    />
                                );
                            })
                    )}

                    {view === 'pricing' && (
                        <>
                            <Box flex={1}>
                                <Text
                                    fontSize={0}
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perHourMorning, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                            <Box flex={1}>
                                <Text
                                    fontSize={0}
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perHourAfternoon, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                            <Box flex={1}>
                                <Text
                                    textAlign="center"
                                    color="primary"
                                    {...PRICE_TEXT_PROPS}
                                >
                                    {format(pricing.perDay, { currency, minimumFractionDigits: 0 })}
                                </Text>
                            </Box>
                        </>
                    )}
                </Box>
            </Box>
        </Touchable>
    );
}

export default DayCell;
