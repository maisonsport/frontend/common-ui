import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import Accordion, { headerBarContainer, nestedHeaderBarcontainer, AccordionProps } from '.';
import Text from '../../atoms/Text';

const commonProps: AccordionProps = {
  children: <></>,
  image: null,
  title: 'Hello world',
};

describe('Accordion', () => {
  test('@prop: title', () => {
    const titleText = 'This is a title!';
    const { getByText } = render(<Accordion {...commonProps} title={titleText} />);

    expect(getByText(titleText)).toBeTruthy();
  });
  test('@prop: titleProps', () => {
    const titleText = 'This is a title!';
    const color = 'aqua';
    const { getByText } = render(<Accordion {...commonProps} title={titleText} titleProps={{ color }} />);

    const el = getByText(titleText);

    expect(el.props.style[0].color).toBe(color);
  });

  test('@prop: onOpen', () => {
    const onOpen = jest.fn().mockName('onOpen');
    const { getByTestId } = render(<Accordion {...commonProps} {...{ onOpen }} />);

    const headerBar = getByTestId(headerBarContainer);

    fireEvent.press(headerBar);

    expect(onOpen).toHaveBeenCalled();
  });
  test('@prop: onClose', () => {
    const onClose = jest.fn().mockName('onClose');
    const { getByTestId } = render(<Accordion {...commonProps} {...{ onClose }} />);

    const headerBar = getByTestId(headerBarContainer);

    fireEvent.press(headerBar);
    expect(onClose).not.toHaveBeenCalled();

    fireEvent.press(headerBar);
    expect(onClose).toHaveBeenCalled();
  });

  test('@prop: openByDefault [true]', () => {
    const contentID = 'accordion-content';
    const content = <Text testID={contentID}>Some text</Text>;
    const { getByTestId } = render(<Accordion {...commonProps} openByDefault>{content}</Accordion>);

    expect(getByTestId(contentID)).toBeTruthy();
  });
  test('@prop: openByDefault [false]', async () => {
    const contentID = 'accordion-content';
    const content = <Text testID={contentID}>Some text</Text>;
    const { getByTestId } = render(<Accordion { ...commonProps}>{content}</Accordion>);

    expect(() => getByTestId(contentID)).toThrow();
  });

  test('@prop: nested [true]', () => {
    const { getByTestId } = render(<Accordion {...commonProps} nested />);

    expect(getByTestId(nestedHeaderBarcontainer)).toBeTruthy();
  });
  test('@prop: nested [false]', () => {
    const { getByTestId } = render(<Accordion {...commonProps} />);

    expect(() => getByTestId(nestedHeaderBarcontainer)).toThrow();
  });

  test('@prop: image', () => {
    const imageTestID = 'image-test-id';
    // @ts-ignore
    const { getByTestId } = render(<Accordion {...commonProps} image={<img alt="" testID={imageTestID} />} />);

    getByTestId(imageTestID);
  });

  test('Nesting accordions', () => {
    const nestedContent = 'Some nested content';
    const nestedAccordion = (
      <Accordion {...commonProps} nested>
        <Text>{nestedContent}</Text>
      </Accordion>
    );
    const { getByTestId, getByText } = render(
      <Accordion { ...commonProps}>
        {nestedAccordion}
      </Accordion>,
    );

    // No nested element shown on initial render
    expect(() => getByTestId(nestedHeaderBarcontainer)).toThrow();

    // Open the parent accordion
    fireEvent.press(getByTestId(headerBarContainer));

    /**
     * - Nested header-bar should not be visible.
     * - The nested accordions 'content should not be visible.
     */
    expect(() => getByTestId(nestedHeaderBarcontainer)).toBeTruthy();
    expect(() => getByText(nestedContent)).toThrow();

    // Open the nested accordion
    fireEvent.press(getByTestId(nestedHeaderBarcontainer));

    // The nested accordions 'content should be visible
    expect(() => getByText(nestedContent)).toBeTruthy();
  });
});
