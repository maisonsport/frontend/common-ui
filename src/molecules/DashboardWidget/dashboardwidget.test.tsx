import React from 'react';
import { render, fireEvent, act } from '@testing-library/react-native';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';

import DashboardWidget, {
  DashboardWidgetProps,

  toggleTestID,
  headingTestID,
  subheadingTestID,
  iconContainerTestID,
  infoIconTestID,
  chevronIconTestID,
} from '.';

const widgetBodyTestID = 'collapsed-content';

const widgetBody = <Box testID={widgetBodyTestID} />
const heading = 'Some test heading';
const subheading = 'Some test subheading';

const defaultProps: DashboardWidgetProps = {
  heading,
  subheading,
};

describe('DashboardWidget', () => {
  beforeEach(() => jest.useFakeTimers());
  afterEach(() => jest.useRealTimers());

  describe('@prop: heading', () => {
    test('As string', () => {
      const { getByText, getByTestId } = render(
        <DashboardWidget {...defaultProps} />
      );
      const elementByText = getByText(heading);
      const elementByID = getByTestId(headingTestID);

      expect(elementByText).toBeTruthy();
      expect(elementByID.children[0]).toBe(heading);
    });
    test('As component', () => {
      const tempText = 'some temp heading';
      const testID = 'the-temp-test-id';
      const { getByText, getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          heading={() => <Text testID={testID}>{tempText}</Text>}
        />
      );
      const elementByText = getByText(tempText);
      const elementByID = getByTestId(testID);

      expect(elementByText).toBeTruthy();
      expect(elementByID.children[0]).toBe(tempText);
    });
  });

  describe('@prop: subheading', () => {
    test('As string', () => {
      const { getByText, getByTestId } = render(
        <DashboardWidget {...defaultProps} />
      );
      const elementByText = getByText(subheading);
      const elementByID = getByTestId(subheadingTestID);

      expect(elementByText).toBeTruthy();
      expect(elementByID.children[0]).toBe(subheading);
    });
    test('As component', () => {
      const tempText = 'some temp subheading';
      const testID = 'the-temp-test-id';
      const { getByText, getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          subheading={() => <Text testID={testID}>{tempText}</Text>}
        />
      );
      const elementByText = getByText(tempText);
      const elementByID = getByTestId(testID);

      expect(elementByText).toBeTruthy();
      expect(elementByID.children[0]).toBe(tempText);
    });
  });

  describe('@prop: Icon', () => {
    test('Not defined', () => {
      const { getByTestId } = render(
        <DashboardWidget {...defaultProps} />
      );

      expect(() => getByTestId(iconContainerTestID)).toThrow();
    });
    test('As Chevron', () => {
      const { getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          Icon={() => <DashboardWidget.ChevronIcon />}
        />
      );

      expect(getByTestId(chevronIconTestID)).toBeTruthy();
    });
    test('As Info', () => {
      const { getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          Icon={() => <DashboardWidget.InfoIcon />}
        />
      );

      expect(getByTestId(infoIconTestID)).toBeTruthy();
    });
    test('As custom', () => {
      const tempTestID = 'temp-icon-id';
      const { getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          Icon={() => <Text testID={tempTestID} />}
        />
      );

      expect(getByTestId(tempTestID)).toBeTruthy();
    });
    test('With isOpen argument', () => {
      const openTestID = 'temp-open';
      const closedTestID = 'temp-closed';
      const { getByTestId } = render(
        <DashboardWidget
          {...defaultProps}
          Icon={(isOpen) => isOpen ? <Text testID={openTestID} /> : <Text testID={closedTestID} />}
          children={widgetBody}
        />
      );
      const toggleElement = getByTestId(toggleTestID);

      expect(getByTestId(closedTestID)).toBeTruthy();

      act(() => fireEvent.press(toggleElement));
      expect(getByTestId(openTestID)).toBeTruthy();
    });
  });
  describe('@prop: onToggle', () => {
    describe('Should not be called', () => {
      test('No children', () => {
        const func = jest.fn();
        const { getByTestId } = render(
          <DashboardWidget {...defaultProps} />
        );
        const toggleElement = getByTestId(toggleTestID);

        expect(func).not.toHaveBeenCalled();

        expect(() => fireEvent.press(toggleElement)).toThrow();

        expect(func).not.toHaveBeenCalled();
      });
      test('Has children but @prop:alwaysOpen is present', () => {
        const func = jest.fn();
        const { getByTestId } = render(
          <DashboardWidget {...defaultProps} children={widgetBody} alwaysOpen />
        );
        const toggleElement = getByTestId(toggleTestID);

        expect(func).not.toHaveBeenCalled();

        expect(() => fireEvent.press(toggleElement)).toThrow();

        expect(func).not.toHaveBeenCalled();
      });
    });
    test('Should be called', () => {
      const func = jest.fn();
      const { getByTestId } = render(
        <DashboardWidget {...defaultProps} children={widgetBody} />
      );
      const toggleElement = getByTestId(toggleTestID);

      expect(func).not.toHaveBeenCalled();

      act(() => fireEvent.press(toggleElement));
      expect(func).not.toHaveBeenCalledWith(true);

      act(() => fireEvent.press(toggleElement));
      expect(func).not.toHaveBeenCalledWith(false);
    });
  });
  describe('@prop: children', () => {
    test('Should be toggled', () => {
      const { getByTestId } = render(
        <DashboardWidget {...defaultProps} children={widgetBody} />
      );
      const toggleElement = getByTestId(toggleTestID);

      expect(() => getByTestId(widgetBodyTestID)).toThrow();

      act(() => fireEvent.press(toggleElement));
      expect(getByTestId(widgetBodyTestID)).toBeTruthy();

      act(() => fireEvent.press(toggleElement));
      expect(() => getByTestId(widgetBodyTestID)).toThrow();
    });
    test('Should always be visible', () => {
      const { getByTestId } = render(
        <DashboardWidget {...defaultProps} children={widgetBody} alwaysOpen />
      );

      expect(getByTestId(widgetBodyTestID)).toBeTruthy();
    });
  });
});
