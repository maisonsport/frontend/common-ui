import React from 'react';
import { format, isToday } from 'date-fns';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';
import CalendarMenuAction, { DEFAULT_ACTION_SIZE } from '../../molecules/CalendarMenuAction';

import { TranslatorFunc } from '../../types/global';
import { MenuActions } from '../../organisms/CalendarMenu';


export interface Props {
    date: Date;

    t: TranslatorFunc;
    onActionPress: (action: MenuActions) => void;
}

const DateHeading = ({ date, t, onActionPress }: Props) => {
    if (!date) return null;

    return (
        <Box
            padding={2}
            paddingTop={3}
            paddingBottom={3}
            backgroundColor="white"
            borderBottomWidth={1 / 2}
            borderBottomColor="grey"

            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
        >
            <Text variant="h3" padding={0} margin={0} fontSize={2} color="textGrey">
                {isToday(date) ? t('text.general.today') : format(date, 'dd/MM/yyyy')}
            </Text>
            <Box flexGrow={1} justifyContent="flex-end" flexDirection="row">
                <Touchable
                    padding={2}
                    marginRight={2}
                    borderRadius={5}
                    borderWidth={1/2}
                    borderColor="lightPrimary"
                    flexDirection="row"
                    justifyContent="space-between"
                    alignItems="center"
                    onPress={() => onActionPress('new')}
                >
                    <CalendarMenuAction.Calendar labelTouchable={false} t={t} variant="light" size={DEFAULT_ACTION_SIZE * 0.5} />
                </Touchable>
                <Touchable
                    padding={2}
                    borderWidth={1/2}
                    borderRadius={5}
                    borderColor="lightPrimary"
                    flexDirection="row"
                    justifyContent="space-between"
                    alignItems="center"
                    onPress={() => onActionPress('settings')}
                >
                    <CalendarMenuAction.Settings labelTouchable={false} t={t} variant="light" size={DEFAULT_ACTION_SIZE * 0.5} />
                </Touchable>
            </Box>
        </Box>
    );
}

export default DateHeading;
