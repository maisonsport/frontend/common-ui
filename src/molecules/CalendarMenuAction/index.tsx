import React from 'react';
import { Image, ImageSourcePropType } from 'react-native';

import Box from '../../atoms/Box';
import Text from '../../atoms/Text';
import Touchable from '../../atoms/Touchable';

import Theme from '../../theme/index';
import { CalendarSyncStatus } from '../../types/calendar';

import { TranslatorFunc } from '../../types/global';

export const DEFAULT_ACTION_SIZE = 50;

interface Props {
    onPress?: () => any;
    icon?: React.ReactElement;
    size?: number;
    variant?: 'light' | 'dark';
    label?: string;
    labelTouchable?: boolean;
    color?: string;
    textBackdropColor?: string;
    [key: string]: any;
}

interface PropsWithTranslator extends Props {
    t: TranslatorFunc;
}

const ActionButton = ({
    icon = <></>,
    onPress,
    variant = 'dark',
    size = DEFAULT_ACTION_SIZE,
    label,
    color,
    textBackdropColor = 'transparent',
    labelTouchable = true,
    ...props
}: Props): JSX.Element => {
    const Component = !!onPress ? Touchable : Box;
    const LabelComponent = labelTouchable ? Touchable : Box;
    return (
        <Box flexDirection="row" alignItems="center" justifyContent="flex-end" {...props}>
            {!!label && (
                <LabelComponent onPress={onPress} borderRadius={2.5} backgroundColor={textBackdropColor} padding={1} marginRight={1} alignItems="center">
                    <Text
                        color={props.textColor ? props.textColor : (variant === 'dark' ? Theme.colors.white : Theme.colors.primary)}
                        fontSize={1}
                        fontWeight={5}
                        padding={0}
                    >
                        {label}
                    </Text>
                </LabelComponent>
            )}

            <Component
                height={size}
                width={size}
                borderRadius={size / 2}
                padding={1}
                alignItems="center"
                justifyContent="center"
                backgroundColor={color ? color : (variant === 'dark' ? 'primary' : 'white')}
                borderWidth={variant === 'light' ? 1 / 2 : 0}
                borderColor="primary"
                onPress={onPress}
                style={{
                    shadowColor: Theme.colors.black,
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,

                    elevation: 5,
                }}
            >
                {icon}
            </Component>
        </Box>
    )
}

export default ActionButton;

ActionButton.Plus = ({ variant = 'dark', size = DEFAULT_ACTION_SIZE, ...props }: Props) => {
    const source = variant === 'dark' ? require('../../assets/images/+_white.png') : require('../../assets/images/+_primary.png')
    return (
        <ActionButton
            icon={(
                <Image
                    source={source}
                    style={{
                        height: Math.floor(size * 0.45),
                        width: Math.floor(size * 0.45),
                    }}
                />
            )}
            {...{ variant, size, ...props }}
        />
    )
}

interface SyncActionProps extends PropsWithTranslator { calendarSyncStatus: CalendarSyncStatus; }
ActionButton.Sync = ({ variant = 'dark', size = DEFAULT_ACTION_SIZE, t, ...props }: SyncActionProps) => {
    let text: string = '';
    let iconSource: ImageSourcePropType|null = null;
    let textColor: string = '';
    let textBackdropColor: string = '';
    let color: string = '';
    switch (props.calendarSyncStatus) {
        case 'CONNECTED':
            text = t('actions.calendar_external_connected');
            iconSource = require('../../assets/images/complete-green.png');
            textBackdropColor = '#E9FFE0';
            color = '#E9FFE0';
            textColor = '#243922';
            break;
        case 'REQUIRES_AUTHENTICATION':
            text = t('actions.calendar_external_requires_authentication');
            iconSource = require('../../assets/images/exclamation.png');
            textBackdropColor = Theme.colors.lightRed;
            color = Theme.colors.lightRed;
            textColor = Theme.colors.white;
            break;
        default:
        case 'NOT_CONNECTED':
            text = t('actions.calendar_external_not_connected');
            iconSource = require('../../assets/images/import-white.png');
            textBackdropColor = Theme.colors.primary;
            color = Theme.colors.primary;
            textColor = Theme.colors.white;
            break;
    }
    return (
        <ActionButton
            icon={iconSource ? (
                <Image
                    source={iconSource}
                    style={{
                        height: Math.floor(size * 0.45),
                        width: Math.floor(size * 0.45),
                    }}
                />
            ) : <></>}
            label={text}
            {...{ variant, size, color, textColor, textBackdropColor, ...props }}
        />
    )
}

ActionButton.Settings = ({ variant = 'dark', size = DEFAULT_ACTION_SIZE, t, ...props }: PropsWithTranslator) => {
    const source = variant === 'dark' ? require('../../assets/images/cog_white.png') : require('../../assets/images/cog_primary.png');
    return (
        <ActionButton
            icon={(
                <Image
                    source={source}
                    style={{
                        height: Math.floor(size * 0.45),
                        width: Math.floor(size * 0.45),
                    }}
                />
            )}
            label={t('navigation.scenes.settings')}
            {...{ variant, size, ...props }}
        />
    )
}

ActionButton.Close = ({ variant = 'dark', size = DEFAULT_ACTION_SIZE, ...props }: Props) => {
    const source = variant === 'dark' ? require('../../assets/images/x_white.png') : require('../../assets/images/x_primary.png');
    return (
        <ActionButton
            color={Theme.colors.lightRed}
            icon={(
                <Image
                    source={source}
                    style={{
                        height: size * 0.4,
                        width: size * 0.4,
                    }}
                />
            )}
            {...{ variant, size, ...props }}
        />
    )
}

ActionButton.Calendar = ({ variant = 'dark', size = DEFAULT_ACTION_SIZE, t, ...props }: PropsWithTranslator) => {
    const source = variant === 'dark' ? require('../../assets/images/calendar_white.png') : require('../../assets/images/calendar_primary.png');

    return (
        <ActionButton
            icon={(
                <Image
                    source={source}
                    style={{
                        height: Math.floor(size * 0.45),
                        width: Math.floor(size * 0.45),
                    }}
                />
            )}
            label={t('actions.calendar_create_new')}
            {...{ variant, size, ...props }}
        />
    )
}
